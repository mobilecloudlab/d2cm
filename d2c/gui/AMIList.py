"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import wx

class AMIList(wx.ListCtrl):
    
    def __init__(self, *args, **kwargs):
        wx.ListCtrl.__init__(self, *args, **kwargs)
        
        self.InsertColumn(0, 'AMI', width=75)
        self.InsertColumn(1, 'Source Image', width=200)
        self.InsertColumn(2, 'Status', width=110)
        self.InsertColumn(3, 'Created', width=110)
        
        self.amis = {}
        
        if kwargs.has_key('amis'):
            self.setAMIs(kwargs['amis'])
        
    def addAMIEntry(self, ami):
        idx = self.Append((ami.id,ami.srcImg))
        self.amis[idx] = ami
    
    def getAMIs(self):
        return self.amis.values()
    
    def setAMIs(self, amis): 
        self.DeleteAllItems()
        self.amis.clear()
        
        for ami in amis:
            self.addAMIEntry(ami)
            
    def getSelectedAMIs(self):
        #TODO return more than one ami if selected
        i = self.GetFirstSelected();
        
        if i < 0:
            return []
        else:
            return [self.amis[i]]