"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import wx
from wx.lib.pubsub import Publisher as pub
from .AMIPanel import AMIPanel
import pkg_resources
from .RawImagePanel import RawImagePanel
from .DeploymentTab import DeploymentTab
from .ImageTab import ImageTab
from .NewAMIWizard import KernelPanel, RamdiskPanel
from d2c.model.Kernel import Kernel
from d2c.model.Ramdisk import Ramdisk
import d2c.controller.util as util
from d2c.model.AWSCred import AWSCred
from d2c.CloudUtility import CloudUtility

class MainTabContainer(wx.Panel):
    
    def __init__(self, *args, **kwargs):
        wx.Panel.__init__(self, *args, **kwargs)
        
class Gui(wx.Frame):    
    
    _ID_LISTBOX = 1
    _ID_ADD_IMAGE = 2
    
    #Labels
    LABEL_SOURCE_IMAGES = "Source Images"
    LABEL_AMIS = "AMIs"
    
    ID_ADD_DEPLOYMENT = 3
    ID_CONF = 2
    ID_CLOUD = 5
    
    def __init__(self, dao, parent=None, id=-1, title='D2C'):
        wx.Frame.__init__(self, parent, id, title, size=(750, 550))

        self.Center()

        self.__initMenuBar()

        toolbar = self.CreateToolBar()
        
        toolbar.AddLabelTool(self.ID_CONF, '', wx.Bitmap(pkg_resources.resource_filename(__package__, "icons/keys-icon.png")))
        toolbar.AddLabelTool(self.ID_CLOUD, '', wx.Bitmap(pkg_resources.resource_filename(__package__, "icons/cloud-hd-icon.png")))
        toolbar.AddLabelTool(self.ID_ADD_DEPLOYMENT, '', wx.Bitmap(pkg_resources.resource_filename(__package__, "icons/network-icon.png")))
     
        self.tabContainer = wx.Notebook(self, -1, style=wx.NB_TOP)
        
        self.imageTab = ImageTab(dao, self.tabContainer, -1)
        self.tabContainer.AddPage(self.imageTab, "Images")
        
        self.imagePanel = RawImagePanel(self.tabContainer, -1)
        #self.tabContainer.AddPage(self.imagePanel, "Source Images")
        
        self.amiPanel = AMIPanel(dao, self.tabContainer, -1)
        self.tabContainer.AddPage(self.amiPanel, "AMIs")
        
        self.deploymentPanel = DeploymentTab(dao, self.tabContainer, -1)
        self.tabContainer.AddPage(self.deploymentPanel, "Deployment Templates/Deployments")
        
        #Add Kernel Panel
        self.kernelPanel = KernelTapPanel(dao, self.tabContainer, -1)
        self.tabContainer.AddPage(self.kernelPanel, "Kernels")
        self.kernelPanel.chooseButton.Hide()
        self.kernelPanel.backButton.Hide()
        
        #Add Ramdisk Panel
        self.ramdiskPanel = RamdiskTabPanel(dao, self.tabContainer, -1)
        self.tabContainer.AddPage(self.ramdiskPanel, "Ramdisks")
        self.ramdiskPanel.chooseButton.Hide()
        self.ramdiskPanel.backButton.Hide()
        
        #TODO move to controller
        pub.subscribe(self.__createAMI, "CREATE AMI")
        
    def __initMenuBar(self):
        
        menubar = wx.MenuBar()
        file = wx.Menu()
        quit = wx.MenuItem(file, 1, '&Quit\tCtrl+Q')
        file.AppendItem(quit)

        menubar.Append(file, '&File')
        self.SetMenuBar(menubar)

        self.Bind(wx.EVT_MENU, self.OnQuit, id=1)
        
    def addPanel(self, label, panel):
        self._items.Append(label)
        self._containerPanel.addPanel(label, panel)
    
    def bindAddDeploymentTool(self, method):
        self.Bind(wx.EVT_TOOL, method, id=self.ID_ADD_DEPLOYMENT)
        
    def bindConfTool(self, method):
        self.Bind(wx.EVT_TOOL, method, id=self.ID_CONF)
        
    def bindCloudTool(self, method):
        self.Bind(wx.EVT_TOOL, method, id=self.ID_CLOUD)
        
    #TODO move to controller
    def __createAMI(self, _):
        self.tabContainer.ChangeSelection(2)
        
    def setSelection(self, label):
        self._items.SetStringSelection(label)
        self._containerPanel.showPanel(self._items.GetStringSelection())
    
    def getImagePanel(self):
        return self._containerPanel.getPanel(self.LABEL_SOURCE_IMAGES)
    
    def getAMIPanel(self):
        return self._containerPanel.getPanel(self.LABEL_AMIS)
    
    def OnQuit(self, event):
        self.Close()

    def OnListSelect(self, event):
        self._containerPanel.showPanel(self._items.GetStringSelection())

class KernelTapPanel(KernelPanel):
    def __init__(self, dao, *args, **kwargs):
        self.dao = dao
        
        KernelPanel.__init__(self, *args, **kwargs)
        
        self.cloudCbx = wx.ComboBox(self, -1, choices=[c.name for c in dao.getClouds()])
        self.Bind(wx.EVT_COMBOBOX , self.onCloudSelected, self.cloudCbx)
        
        self.sizer.Insert(1,wx.StaticText(self, -1, "Select cloud:"), 0 , wx.ALL, 2)
        self.sizer.Insert(2, self.cloudCbx, 0 , wx.ALL, 2)
        
        #self.sizer.Add(wx.StaticText(self, -1, "Select cloud:"), 0 , wx.ALL, 2)
        #self.sizer.Add(self.cloudCbx, 0 , wx.ALL, 2)
        
        self.splitter = wx.SplitterWindow(self, -1)
        self.splitter.SetMinimumPaneSize(150)
        
        self.sizer.Add(self.splitter, 1, wx.EXPAND)
        
        self.bottomPanel = wx.Panel(self, -1)
        self.bottomPanel.hbox = wx.BoxSizer(wx.HORIZONTAL)
        self.bottomPanel.addButton = wx.Button(self.bottomPanel, -1, 'Add Kernel')
        
        self.bottomPanel.deleteButton = wx.Button(self.bottomPanel, -1, 'Delete Kernel')
        
        self.bottomPanel.hbox.Add(self.bottomPanel.addButton, 0, wx.ALL, 5)
        self.bottomPanel.hbox.Add(self.bottomPanel.deleteButton, 0, wx.ALL, 5)
        
        self.bottomPanel.SetSizer(self.bottomPanel.hbox)
        self.bottomPanel.SetBackgroundColour('GREY')
        
        self.sizer.Add(self.bottomPanel, 0, wx.EXPAND)
        
        self.bottomPanel.addButton.Bind(wx.EVT_BUTTON, self.addKernel)
        self.bottomPanel.deleteButton.Bind(wx.EVT_BUTTON, self.deleteKernel)
        
        util.createEmptyChecker(self.bottomPanel.addButton,  self.cloudCbx)
    
    def setKernelList(self):
        self.kernelList.setItems(self.cloud.kernels)
    
    def onCloudSelected(self, _):
        self.cloud = self.dao.getCloud(self.cloudCbx.GetValue())
        self.setKernelList()
    
    def addKernel(self, _):
        dialog = AddKernelDialog(self.dao, self.cloud, None, -1, 'Add Kernel', size=(400,400))
        if dialog.ShowModal() == wx.ID_OK:
            kernel = Kernel(dialog.aki.GetValue(), self.dao.getArchitecture(dialog.architecture.GetValue()), None, self.cloud)
            self.dao.add(kernel)
            self.setKernelList()
        
        dialog.Destroy()
        
    def deleteKernel(self, _):
        kernels = self.kernelList.getSelectedItems()
        if len(kernels) != 1:
            wx.MessageBox("Please select one Kernel", 'Info')
            return
        
        kernel = kernels[0]
        
        if kernel.aki == "eki-3EB4165A":
            wx.MessageBox("Cannot delete Default Kernel", 'Info')
            return
        else:
            dlg = wx.MessageDialog(self, "Are you sure that you want to delete the Kernel: " + kernel.aki + "?", "Info", wx.OK|wx.CANCEL|wx.ICON_EXCLAMATION)
            result = dlg.ShowModal()
            dlg.Destroy()
            if result == wx.ID_OK:
                self.dao.delete(kernel)
                self.setKernelList()

class RamdiskTabPanel(RamdiskPanel):
    def __init__(self, dao, *args, **kwargs):
        self.dao = dao
        
        RamdiskPanel.__init__(self, *args, **kwargs)
        
        self.cloudCbx = wx.ComboBox(self, -1, choices=[c.name for c in dao.getClouds()])
        self.Bind(wx.EVT_COMBOBOX , self.onCloudSelected, self.cloudCbx)
        
        self.splitter = wx.SplitterWindow(self, -1)
        self.splitter.SetMinimumPaneSize(150)
        
        self.sizer.Insert(1,wx.StaticText(self, -1, "Select cloud:"), 0 , wx.ALL, 2)
        self.sizer.Insert(2, self.cloudCbx, 0 , wx.ALL, 2)
        
        #self.sizer.Add(wx.StaticText(self, -1, "Select cloud:"), 0 , wx.ALL, 2)
        #self.sizer.Add(self.cloudCbx, 0 , wx.ALL, 2)
        
        self.sizer.Add(self.splitter, 1, wx.EXPAND)
                
        self.bottomPanel = wx.Panel(self, -1)
        self.bottomPanel.hbox = wx.BoxSizer(wx.HORIZONTAL)
        self.bottomPanel.addButton = wx.Button(self.bottomPanel, -1, 'Add Ramdisk')
        self.bottomPanel.deleteButton = wx.Button(self.bottomPanel, -1, 'Delete Ramdisk')
        
        self.bottomPanel.hbox.Add(self.bottomPanel.addButton, 0, wx.ALL, 5)
        self.bottomPanel.hbox.Add(self.bottomPanel.deleteButton, 0, wx.ALL, 5)
        
        self.bottomPanel.SetSizer(self.bottomPanel.hbox)
        self.bottomPanel.SetBackgroundColour('GREY')
        
        self.sizer.Add(self.bottomPanel, 0, wx.EXPAND)
        
        self.bottomPanel.addButton.Bind(wx.EVT_BUTTON, self.addRamdisk)
        self.bottomPanel.deleteButton.Bind(wx.EVT_BUTTON, self.deleteRamdisk)
        
        util.createEmptyChecker(self.bottomPanel.addButton,  self.cloudCbx)
    
    def setRamdisklList(self):
        self.rdList.setItems(self.cloud.ramdisks)
    
    def onCloudSelected(self, _):
        self.cloud = self.dao.getCloud(self.cloudCbx.GetValue())
        self.setRamdisklList()

    def addRamdisk(self, _):
        dialog = AddRamdiskDialog(self.dao, self.cloud, None, -1, 'Add Ramdisk', size=(400,400))
        if dialog.ShowModal() == wx.ID_OK:
            ramdisk = Ramdisk(dialog.id.GetValue(), self.cloud, self.dao.getArchitecture(dialog.architecture.GetValue()))
            self.dao.add(ramdisk)
            self.setRamdisklList()
        
        dialog.Destroy()
        
    def deleteRamdisk(self, _):
        ramdisks = self.rdList.getSelectedItems()
        if len(ramdisks) != 1:
            wx.MessageBox("Please select one Ramdisk", 'Info')
            return
        
        ramdisk = ramdisks[0]
        
        if ramdisk.id == "eri-83141744":
            wx.MessageBox("Cannot delete Default Ramdisk", 'Info')
            return
        else:
            dlg = wx.MessageDialog(self, "Are you sure that you want to delete the Ramdisk: " + ramdisk.id + "?", "Info", wx.OK|wx.CANCEL|wx.ICON_EXCLAMATION)
            result = dlg.ShowModal()
            dlg.Destroy()
            if result == wx.ID_OK:
                self.dao.delete(ramdisk)
                self.setRamdisklList()


class AddKernelDialog(wx.Dialog):
    
    def __init__(self, dao, cloud, *args, **kwargs):
        wx.Dialog.__init__(self, *args, **kwargs)
        
        self.dao = dao
        self.cloud = cloud
        
        #self.dao.getAWSCred(name):
        #self.dao.getAWSCred(name)
        
        #TODO: Uncomment when retrieving data from the cloud - 1
        #self.credential = wx.ComboBox(self, -1, choices=[c.name for c in self.dao.getCloudCreds()])
        #self.Bind(wx.EVT_COMBOBOX , self.onCredentialSelected, self.credential)
        
        self.aki = wx.TextCtrl(self, -1, size=(100, -1))
        self.architecture = wx.ComboBox(self, -1, choices=[c.arch for c in self.dao.getArchitectures()])
        
        self.createButton = wx.Button(self, -1, "Add")
        self.createButton.Bind(wx.EVT_BUTTON, self.onCreate)
        
        util.createEmptyChecker(self.createButton, self.aki, self.architecture)
        
        self.cancelButton = wx.Button(self, wx.ID_CANCEL, "Cancel")
        
        vbox = wx.BoxSizer(wx.VERTICAL)
        
        #TODO: Uncomment when retrieving data from the cloud - 1
        #vbox.Add(wx.StaticText(self, -1, "Credentials:"), 0 , wx.ALL, 2)
        #vbox.Add(self.credential, 0, wx.EXPAND | wx.ALL, 2)
        
        vbox.Add(wx.StaticText(self, -1, "Kernel ID:"), 0 , wx.ALL, 2)
        vbox.Add(self.aki, 0, wx.EXPAND | wx.ALL, 2)
        
        vbox.Add(wx.StaticText(self, -1, "Architecture:"), 0 , wx.ALL, 2)
        vbox.Add(self.architecture, 0, wx.EXPAND | wx.ALL, 2)
        
        vbox.Add(wx.Panel(self, -1, size=(-1, 10)), 0, wx.EXPAND)
        vbox.Add(self.createButton, 0, wx.ALIGN_RIGHT | wx.ALL, 2)
        vbox.Add(self.cancelButton, 0, wx.ALIGN_RIGHT | wx.ALL, 2)
        
        self.SetSizer(vbox)
    
    def onCreate(self, _):
        self.EndModal(wx.ID_OK)
    
    def onCredentialSelected(self, _):
        self.cloudCred = self.dao.getCloudCred(self.credential.GetValue())
        cloudUtil = CloudUtility(self.cloud.serviceURL)
        cloudUtil.getDescribeImages(self.cloudCred.awsCred)
        
class AddRamdiskDialog(wx.Dialog):
    
    def __init__(self, dao, cloud, *args, **kwargs):
        wx.Dialog.__init__(self, *args, **kwargs)
        
        self.dao = dao
        self.cloud = cloud
        
        self.id = wx.TextCtrl(self, -1, size=(100, -1))
        self.architecture = wx.ComboBox(self, -1, choices=[c.arch for c in dao.getArchitectures()])

        self.createButton = wx.Button(self, -1, "Add")
        self.createButton.Bind(wx.EVT_BUTTON, self.onCreate)
        
        util.createEmptyChecker(self.createButton, self.id, self.architecture)
        
        self.cancelButton = wx.Button(self, wx.ID_CANCEL, "Cancel")
        
        vbox = wx.BoxSizer(wx.VERTICAL)
        
        vbox.Add(wx.StaticText(self, -1, "Ramdisk ID:"), 0 , wx.ALL, 2)
        vbox.Add(self.id, 0, wx.EXPAND | wx.ALL, 2)
        
        vbox.Add(wx.StaticText(self, -1, "Architecture:"), 0 , wx.ALL, 2)
        vbox.Add(self.architecture, 0, wx.EXPAND | wx.ALL, 2)
        
        vbox.Add(wx.Panel(self, -1, size=(-1, 10)), 0, wx.EXPAND)
        vbox.Add(self.createButton, 0, wx.ALIGN_RIGHT | wx.ALL, 2)
        vbox.Add(self.cancelButton, 0, wx.ALIGN_RIGHT | wx.ALL, 2)
        
        self.SetSizer(vbox)
    
    def onCreate(self, _):
        self.EndModal(wx.ID_OK)
        
    

 



        
