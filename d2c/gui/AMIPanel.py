"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import wx
import d2c.controller.util as util
from d2c.model.SourceImage import Image, SourceImage, DesktopImage, AMI
from .ItemList import ColumnMapper, ItemList
from .ContainerPanel import ContainerPanel
from d2c.controller.AMIController import AMITracker
import time

class AMIPanel(wx.Panel):    
    
    def __init__(self, dao, *args):
        wx.Panel.__init__(self, *args)
        
        self.dao = dao
        
        self.splitter = wx.SplitterWindow(self, -1)
        self.splitter.SetMinimumPaneSize(150)
        vbox = wx.BoxSizer(wx.VERTICAL)

        self.list = ItemList(self.splitter, -1, style=wx.LC_REPORT,
                                 mappers=[ColumnMapper('AMI', lambda r: r.id, defaultWidth=wx.LIST_AUTOSIZE),
                                          ColumnMapper('SourceImage', lambda r: r.srcImg.path, defaultWidth=wx.LIST_AUTOSIZE),
                                          ColumnMapper('Status', lambda r: r.status, defaultWidth=wx.LIST_AUTOSIZE_USEHEADER),
                                          ColumnMapper('Cloud', lambda r: r.cloud.name, defaultWidth=wx.LIST_AUTOSIZE)]
                                 )

        vbox.Add(self.splitter, 1, wx.EXPAND)
        
        #Add -Add AMI- Functionality - Start >>>
        self.bottomPanel = wx.Panel(self, -1)
        self.bottomPanel.hbox = wx.BoxSizer(wx.HORIZONTAL)
        self.bottomPanel.addButton = wx.Button(self.bottomPanel, -1, 'Add AMI')
        self.bottomPanel.deleteButton = wx.Button(self.bottomPanel, -1, 'Delete AMI')
        
        self.bottomPanel.hbox.Add(self.bottomPanel.addButton, 0, wx.ALL, 5)
        self.bottomPanel.hbox.Add(self.bottomPanel.deleteButton, 0, wx.ALL, 5)
        
        self.bottomPanel.SetSizer(self.bottomPanel.hbox)
        self.bottomPanel.SetBackgroundColour('GREY')
        
        vbox.Add(self.bottomPanel, 0, wx.EXPAND)
        
        self.bottomPanel.addButton.Bind(wx.EVT_BUTTON, self.addAMI)
        self.bottomPanel.deleteButton.Bind(wx.EVT_BUTTON, self.deleteAMI)
        
        #Add -Add AMI- Functionality - End <<<
        
        self._logPanel = ContainerPanel(self.splitter, -1)
   
        self.splitter.SplitHorizontally(self.list, self._logPanel)
        self.SetSizer(vbox)
        self.Layout()   

         
    def addLogPanel(self, id):
        self._logPanel.addPanel(id, wx.TextCtrl(self._logPanel, -1, size=(100,100), style=wx.TE_MULTILINE )) #style=wx.TE_READONLY
        
    def showLogPanel(self, id):
        self._logPanel.showPanel(id)
        self.Layout()
        self.Refresh()
        
    def appendLogPanelText(self, logPanelId, text):
        self._logPanel.getPanel(logPanelId).AppendText(str(text) + "\n")
        
    def addAMIEntry(self, ami):
        self.list.addItem(ami)
        
    def setAMIs(self, images): 
        self.list.setItems(images)

    def addAMI(self, _):
        dialog = AddAMIDialog(self.dao, None, -1, 'Add AMI', size=(400,400))
        
        if dialog.ShowModal() == wx.ID_OK:
            imageFullSize = 0;
            deskImg = DesktopImage(None, None, self.dao.getCloud(dialog.cloud.GetValue()), time.time(), imageFullSize, 0, dialog.manifest.GetValue())
            img = Image(None, dialog.manifest.GetValue(), deskImg, [deskImg])
            kernel = [k for k in self.dao.getCloud(dialog.cloud.GetValue()).kernels if  k.aki == dialog.kernels.GetValue()]
            ramdisk = [r for r in self.dao.getCloud(dialog.cloud.GetValue()).ramdisks if  r.id == dialog.ramdisks.GetValue()]
            newAMI = AMI(None, img, dialog.amiId.GetValue(), self.dao.getCloud(dialog.cloud.GetValue()), time.time(), imageFullSize, kernel[0], ramdisk[0])
            #deskImg = DesktopImage(None, None, self.dao.getCloud(dialog.cloud.GetValue()), time.time(), imageFullSize, os.path.getsize(dialog.path.GetValue()), dialog.path.GetValue())
            #img = Image(None, dialog.name.GetValue(), deskImg, [deskImg])
            #self.dao.add(img)
            #self.addImagePanel(ImagePanel(img, self.displayPanel, -1)) 
            self.dao.add(newAMI)
        
        dialog.Destroy()
        self.setAMIs([AMITracker(a) for a in self.dao.getAMIs()])
        
    def deleteAMI(self, _):
        amisList = self.list.getSelectedItems()
        if len(amisList) != 1:
            wx.MessageBox("Please select one Ramdisk", 'Info')
            return
        
        amiTrack = amisList[0]
        
        dlg = wx.MessageDialog(self, "Are you sure that you want to delete the AMI: " + amiTrack.id + "?", "Info", wx.OK|wx.CANCEL|wx.ICON_EXCLAMATION)
        result = dlg.ShowModal()
        dlg.Destroy()
        if result == wx.ID_OK:
            amis = [a for a in self.dao.getAMIs() if a.amiId == amiTrack.id]
            ami = amis[0]
            self.dao.delete(ami)
            self.setAMIs([AMITracker(a) for a in self.dao.getAMIs()])
            #ToDO: Remove AMI From Tree
        
class AddAMIDialog(wx.Dialog):
    
    def __init__(self, dao, *args, **kwargs):
        wx.Dialog.__init__(self, *args, **kwargs)
        
        self.amiId = wx.TextCtrl(self, -1, size=(100, -1))
        self.manifest = wx.TextCtrl(self, -1, size=(100, -1))
        self.cloud = wx.ComboBox(self, -1, choices=[c.name for c in dao.getClouds()])
        
        self.kernels = wx.ComboBox(self, -1, choices=[])
        self.ramdisks = wx.ComboBox(self, -1, choices=[])
        
        self.dao = dao
        #self.browseButton = wx.Button(self, -1, "Browse")
        
        #self.browseButton.Bind(wx.EVT_BUTTON, self.onFindImage)
        #self.cloud.Bind(wx.EVT_BUTTON, self.onFindImage)
        
        self.createButton = wx.Button(self, -1, "Add")
        self.createButton.Bind(wx.EVT_BUTTON, self.onCreate)
        
        self.Bind(wx.EVT_COMBOBOX , self.onCloudSelected, self.cloud)
        
        util.createEmptyChecker(self.createButton, self.amiId, self.manifest, self.cloud, self.kernels, self.ramdisks)
        
        self.cancelButton = wx.Button(self, wx.ID_CANCEL, "Cancel")
        
        vbox = wx.BoxSizer(wx.VERTICAL)
        
        vbox.Add(wx.StaticText(self, -1, "AMI ID:"), 0 , wx.ALL, 2)
        vbox.Add(self.amiId, 0, wx.EXPAND | wx.ALL, 2)
        
        vbox.Add(wx.StaticText(self, -1, "Manifest File:"), 0 , wx.ALL, 2)
        vbox.Add(self.manifest, 0, wx.EXPAND | wx.ALL, 2)
        #vbox.Add(self.browseButton, 0, wx.ALIGN_RIGHT | wx.ALL, 2)
        
        vbox.Add(wx.StaticText(self, -1, "Select associated cloud:"), 0 , wx.ALL, 2)
        vbox.Add(self.cloud, 0, wx.ALL, 2)
        
        vbox.Add(wx.StaticText(self, -1, "Select kernel:"), 0 , wx.ALL, 2)
        vbox.Add(self.kernels, 0, wx.ALL, 2)
        
        vbox.Add(wx.StaticText(self, -1, "Select ramdisk:"), 0 , wx.ALL, 2)
        vbox.Add(self.ramdisks, 0, wx.ALL, 2)
        
        vbox.Add(wx.Panel(self, -1, size=(-1, 10)), 0, wx.EXPAND)
        vbox.Add(self.createButton, 0, wx.ALIGN_RIGHT | wx.ALL, 2)
        vbox.Add(self.cancelButton, 0, wx.ALIGN_RIGHT | wx.ALL, 2)
        
        self.SetSizer(vbox)
    
    def onCreate(self, _):
        self.EndModal(wx.ID_OK)
    
    def onCloudSelected(self, _):
        self.kernels.Clear()
        self.kernels.SetValue("")
        self.ramdisks.Clear()
        self.ramdisks.SetValue("")
        self.kernels.SetItems([k.aki for k in self.dao.getCloud(self.cloud.GetValue()).kernels])
        self.ramdisks.SetItems([r.id for r in self.dao.getCloud(self.cloud.GetValue()).ramdisks])
        
'''
    def onFindImage(self, _):
        dlg = wx.FileDialog(self, "Choose an image", os.getcwd(), "", "*.*", wx.OPEN)
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            self.path.SetValue(path)
        dlg.Destroy()
'''
