"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import wx

class ColumnMapper:
    
    def __init__(self, name, mapFunc, defaultWidth=50):
        self.name = name
        self.map = mapFunc
        self.defaultWidth = defaultWidth

class ItemList(wx.ListCtrl):
    
    def __init__(self, *args, **kwargs):
        
        if not kwargs.has_key('mappers'):
            raise Exception("No Mappers")
        
        self.mappers = kwargs['mappers']
        del kwargs['mappers']
        
        items = []
        if kwargs.has_key('items'):
            items = kwargs['items']
            del kwargs['items']
        
        wx.ListCtrl.__init__(self, *args, **kwargs)
                                           
        for i,colMapper in enumerate(self.mappers):
            self.InsertColumn(i, colMapper.name)
            
        self.items = {}
        
        self.setItems(items)
        
    def addItem(self, item):
        idx = self.Append([colMapper.map(item) for colMapper in self.mappers])
        
        for i,m in enumerate(self.mappers):
            self.SetColumnWidth(i, m.defaultWidth) 
        
        self.items[idx] = item
        self.Layout()
        
    
    def getItems(self):
        return self.items.values()
    
    def clear(self):
        self.DeleteAllItems()
        self.items.clear()
    
    def setItems(self, items): 
        self.clear()
        
        for item in items:
            self.addItem(item)
            
    def getSelectedItems(self):
        #TODO return more than one item if selected
        i = self.GetFirstSelected();
        
        if i < 0:
            return []
        else:
            return [self.items[i]]