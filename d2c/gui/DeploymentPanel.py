"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import wx
from .RoleList import RoleList
from d2c.model.Deployment import DeploymentState
from d2c.gui.ItemList import ItemList, ColumnMapper
from d2c.graph.Grapher import Grapher
import datetime
import time
import threading
import traceback
import sys

def formatTime(t):
    return datetime.datetime.fromtimestamp(t).isoformat()

class DeploymentPanel(wx.Panel):    
    
    '''
    Renders Deployment information
    '''
    FocusFlag = False
    FinishMonitorUpdate = True
    
    def __init__(self, deployment, *args, **kwargs):
        wx.Panel.__init__(self, *args, **kwargs)
        
        self.SetSizer(wx.BoxSizer(wx.VERTICAL))
        
        self.deployment = deployment
        
        label = wx.StaticText(self, -1, deployment.deploymentTemplate.name + ":" + str(deployment.id))
        label.SetFont(wx.Font(20, wx.DEFAULT, wx.DEFAULT, wx.BOLD))
        self.GetSizer().Add(label, 0, wx.BOTTOM | wx.TOP, 10)
        
        self.tabContainer = wx.Notebook(self, -1, style=wx.NB_TOP)
        self.GetSizer().Add(self.tabContainer, 1, wx.ALL | wx.EXPAND, 5)
         
        self.overviewTab = OverviewTab(self.deployment, self.tabContainer, -1)
        self.tabContainer.AddPage(self.overviewTab, "Overview")
        
        self.eventTab = EventTab(self.deployment, self.tabContainer, -1)
        #self.eventTab.update();
        self.tabContainer.AddPage(self.eventTab, "Log / Events")
        
        self.monitorTab = MonitorTab(self.deployment, self.tabContainer, -1)
        self.tabContainer.AddPage(self.monitorTab, "Monitoring")
        
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        self.GetSizer().Add(hbox)
        self.cloneButton = wx.Button(self, -1 , "Clone Deployment")
        hbox.Add(self.cloneButton, 0, wx.ALL, 5)
        
        self.deleteButton = wx.Button(self, -1 , "Delete Deployment")
        hbox.Add(self.deleteButton, 0, wx.ALL, 5)
        
        self.monitorTab.Bind(wx.EVT_SET_FOCUS, self.onSetFocus)
        self.monitorTab.Bind(wx.EVT_KILL_FOCUS, self.onKillFocus)
    
    def onSetFocus(self, event):
        #if self.deployment.state == DeploymentState.ROLES_STARTED and self.FocusFlag == False:
        if self.FocusFlag == False:
            self.FocusFlag = True
            if self.FinishMonitorUpdate == True:
                self.FinishMonitorUpdate = False
                #self.monitorTab.graph()
                t1 = threading.Thread(target=self.monitorTab.update)
                t1.start()
                #t1.join()
                self.FinishMonitorUpdate = True
    
    def onKillFocus(self, event):
        self.FocusFlag = False
        return 0
    
    def showLogPanel(self):
        self.eventTab.showLogPanel()
        
    def appendLogPanelText(self, text):
        self.eventTab.appendLogPanelText(text)
    
    def update(self):
        '''
        Update the panel to reflect the current Deployment
        '''
        self.overviewTab.update()
        self.eventTab.update()
        #self.monitorTab.update()

        
class MonitorTab(wx.Panel):
    '''
    Displays graphs of system resource usage.
    (invalid ->) This will only contain data after completion of a deployment.
    '''
    
    def __init__(self, deployment, *args, **kwargs):
        
        wx.Panel.__init__(self, *args, **kwargs)
        
        self.deployment = deployment
         
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.sizer)
         
        self.sw = wx.ScrolledWindow(self, style=wx.VSCROLL)
        self.sizer.Add(self.sw, 1, wx.ALL | wx.EXPAND, 5)
        
        self.sw.SetScrollbars(1,1,1,1)
        self.sw.SetMinSize((-1, 50))
        
        self.sw.SetScrollbars(20,20,55,40)
        self.sw.SetSizer(wx.BoxSizer(wx.VERTICAL))
        
        
    def graph(self):
        #grapher = Grapher({"vm":"/home/willmore/.d2c/deployments/22.py/QCW7N1/31/i-4B450863/opt/collectd/var/lib/collectd/dirac.lan/",
        #           "vm2":"/home/willmore/.d2c/deployments/22.py/QCW7N1/32/i-468F08BD/opt/collectd/var/lib/collectd/dirac.lan/"
        #           }, "/tmp")
        
        
        collectdPaths = []
        for role in self.deployment.roles:
            collectdPaths.extend(role.getIntsanceCollectdDirs())
        
        if len(collectdPaths) == 0:
            print "No Data"
            return 0
        
        print "Graph data"
        
        graphMap = {}
        for n,path in enumerate(collectdPaths):
            print "Adding path: " + path
            graphMap["vm"+str(n)] = str(path)

        print "Graphing " + str(graphMap)
        
        #grapher = Grapher(graphMap, "/tmp")
        
        #20080715 21:50
        timeStart = 1216147834.85
        if self.deployment.getRoleStartTime() > timeStart:
            timeStart = self.deployment.getRoleStartTime()
        else:
            #Current time - 1 hour
            timeStart = time.time() - 3600 

        timeEnd = 0
        if self.deployment.getRoleEndTime() == -1:
            timeEnd = time.time()
        else:
            timeEnd = self.deployment.getRoleEndTime()
        
        startString = datetime.datetime.fromtimestamp(timeStart).strftime('%Y%m%d %H:%M')
        endString = datetime.datetime.fromtimestamp(timeEnd).strftime('%Y%m%d %H:%M')
        
        print startString
        print endString
        
        #cpuImg = grapher.generateCPUGraphsAverage(startString, endString)
        #memImg = grapher.generateMemoryGraph(startString, endString)
        
        self.deployment.generateMergedGraphicsFromPathLists(collectdPaths, startString, endString)
        
        return 1
        #self.setMonitorImages()
        """cpuImg = 'CPU.png'
        memImg = 'Memory.png'
        loadImg = 'Load.png'
        
        #TODO: Uncomment when the Network RRD files are collected
        #netImg = grapher.generateNetworkGraph(startString, endString)
        
        # Remove all the previous images from the panel also destroying the window
        self.sw.GetSizer().Clear(1)
        
        bmp = wx.Image(cpuImg,wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        hsizer = wx.BoxSizer(wx.HORIZONTAL)
        hsizer.Add(wx.StaticBitmap(self.sw, -1, bmp))
        self.sw.GetSizer().Add(hsizer)
        
        bmp = wx.Image(memImg,wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        hsizer = wx.BoxSizer(wx.HORIZONTAL)
        hsizer.Add(wx.StaticBitmap(self.sw, -1, bmp))
        self.sw.GetSizer().Add(hsizer)
        
        bmp = wx.Image(loadImg,wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        hsizer = wx.BoxSizer(wx.HORIZONTAL)
        hsizer.Add(wx.StaticBitmap(self.sw, -1, bmp))
        self.sw.GetSizer().Add(hsizer)
        
        self.Layout()"""
    
    def setMonitorImages(self):
        cpuImg = self.deployment.dataDir+'/CPU.png'
        memImg = self.deployment.dataDir+'/Memory.png'
        loadImg = self.deployment.dataDir+'/Load.png'
        
        
        #TODO: Uncomment when the Network RRD files are collected
        #netImg = grapher.generateNetworkGraph(startString, endString)
        
        # Remove all the previous images from the panel also destroying the window
        self.sw.GetSizer().Clear(1)
        
        bmp = wx.Image(cpuImg,wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        hsizer = wx.BoxSizer(wx.HORIZONTAL)
        hsizer.Add(wx.StaticBitmap(self.sw, -1, bmp))
        self.sw.GetSizer().Add(hsizer)
        
        bmp = wx.Image(memImg,wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        hsizer = wx.BoxSizer(wx.HORIZONTAL)
        hsizer.Add(wx.StaticBitmap(self.sw, -1, bmp))
        self.sw.GetSizer().Add(hsizer)
        
        bmp = wx.Image(loadImg,wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        hsizer = wx.BoxSizer(wx.HORIZONTAL)
        hsizer.Add(wx.StaticBitmap(self.sw, -1, bmp))
        self.sw.GetSizer().Add(hsizer)
        
        self.Layout()
            

        
    def update(self):
        res = self.graph()
        if res:
            wx.CallAfter(self.setMonitorImages)
        

class EventTab(wx.Panel):
    '''
    Displays Deployment events and log.
    '''   
        
    def __init__(self, deployment, *args, **kwargs):
        
        wx.Panel.__init__(self, *args, **kwargs)    
        
        self.deployment = deployment
        self.SetSizer(wx.BoxSizer(wx.VERTICAL))
        
        self.eventLabel = wx.StaticText(self, -1, label="Events")
        self.eventLabel.SetFont(wx.Font(wx.DEFAULT, wx.DEFAULT, wx.DEFAULT, wx.BOLD))
        self.GetSizer().Add(self.eventLabel, 0, wx.ALL, 2)
        
        self.eventList = ItemList(self, -1, 
                                  mappers=[ColumnMapper('Event', lambda e: e.state, defaultWidth=wx.LIST_AUTOSIZE),
                                           ColumnMapper('Time', lambda e: formatTime(e.time), defaultWidth=wx.LIST_AUTOSIZE)],
                                  style=wx.LC_REPORT)
        
        
        
        
        
        self.GetSizer().Add(self.eventList, 0, wx.ALL | wx.EXPAND, 2)
        
        self.logPanel = wx.TextCtrl(self, -1, size=(100,100), style=wx.TE_MULTILINE )
        self.logLabel = wx.StaticText(self, -1, label="Log")
        self.logLabel.SetFont(wx.Font(wx.DEFAULT, wx.DEFAULT, wx.DEFAULT, wx.BOLD))
        self.GetSizer().Add(self.logLabel, 0, wx.ALL, 2)
        self.logLabel.Hide()
        self.logPanel.Hide() # logPanel will be shown later on demand
        self.GetSizer().Add(self.logPanel, 1, wx.BOTTOM | wx.EXPAND, 5)
        
        self.eventList.setItems(self.deployment.stateEvents)

        for event in self.eventList.getItems():
            self.appendLogPanelText(event)
        
        
    def update(self):
        self.eventList.setItems(self.deployment.stateEvents)
    
    def appendLogPanelText(self, text):
        self.logPanel.AppendText(str(text) + "\n")
            
    def showLogPanel(self):
        self.logPanel.Show()
        self.logLabel.Show()
        self.Layout()
        
class OverviewTab(wx.Panel):
    '''
    Displays basic information about the Deployment.
    '''
    
    def __init__(self, deployment, *args, **kwargs):
        
        wx.Panel.__init__(self, *args, **kwargs)
        
        self.deployment = deployment
        self.SetSizer(wx.BoxSizer(wx.VERTICAL))
                
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.GetSizer().Add(sizer, 0, wx.BOTTOM | wx.TOP, 5)
        label = wx.StaticText(self, -1, 'Cloud')
        label.SetFont(wx.Font(wx.DEFAULT, wx.DEFAULT, wx.DEFAULT, wx.BOLD))
        sizer.Add(label, 0, wx.RIGHT| wx.LEFT, 5)
        self.cloudField = wx.StaticText(self, -1, str(deployment.cloud.name))
        sizer.Add(self.cloudField, 0, wx.ALIGN_CENTER)
        
        self.sizeSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.GetSizer().Add(self.sizeSizer, 0, wx.BOTTOM, 5)
        label = wx.StaticText(self, -1, 'Problem Size')
        label.SetFont(wx.Font(wx.DEFAULT, wx.DEFAULT, wx.DEFAULT, wx.BOLD))
        self.sizeSizer.Add(label, 0, wx.RIGHT| wx.LEFT, 5)
        self.sizeField = wx.StaticText(self, -1, str(deployment.problemSize))
        self.sizeSizer.Add(self.sizeField, 0, wx.ALIGN_CENTER)
        
        self.statusSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.GetSizer().Add(self.statusSizer, 0, wx.BOTTOM, 5)
        label = wx.StaticText(self, -1, 'Status')
        label.SetFont(wx.Font(wx.DEFAULT, wx.DEFAULT, wx.DEFAULT, wx.BOLD))
        self.statusSizer.Add(label, 0, wx.RIGHT | wx.LEFT, 5)
        self.statusField = wx.StaticText(self, -1, deployment.state)
        self.statusSizer.Add(self.statusField, 0, wx.ALIGN_CENTER)
    
    
        self.datadirSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.GetSizer().Add(self.datadirSizer, 0, wx.BOTTOM, 5)
        label = wx.StaticText(self, -1, 'Work directory')
        label.SetFont(wx.Font(wx.DEFAULT, wx.DEFAULT, wx.DEFAULT, wx.BOLD))
        self.datadirSizer.Add(label, 0, wx.RIGHT | wx.LEFT, 5)
        self.datadirField = wx.HyperlinkCtrl(self, -1, label=deployment.dataDir, url="file://"+deployment.dataDir )
        self.datadirSizer.Add(self.datadirField, 0, wx.ALIGN_CENTER)              

        
        label = wx.StaticText(self, -1, 'Roles')
        label.SetFont(wx.Font(wx.DEFAULT, wx.DEFAULT, wx.DEFAULT, wx.BOLD))
        self.GetSizer().Add(label, 0, wx.BOTTOM | wx.LEFT, 5)
        self.roles = RoleList(self, -1, items=deployment.roles)
        self.GetSizer().Add(self.roles, 0, wx.ALL | wx.EXPAND, 5)
        
        self.dataPanel = wx.Panel(self,-1)
        self.GetSizer().Add(self.dataPanel)
        self.dataPanel.SetSizer(wx.BoxSizer(wx.VERTICAL))
        self.dataPanel.Hide()
        
        label = wx.StaticText(self.dataPanel, -1, 'Role Data')
        label.SetFont(wx.Font(wx.DEFAULT, wx.DEFAULT, wx.DEFAULT, wx.BOLD))
        self.dataPanel.GetSizer().Add(label, 0, wx.BOTTOM, 5)
        
        for role in self.deployment.roles:
            hbox = wx.BoxSizer(wx.HORIZONTAL)
            label = wx.StaticText(self.dataPanel, -1, role.getName())
            hbox.Add(label, 0, wx.EXPAND | wx.ALL, 2)
            hbox.Add(wx.StaticText(self.dataPanel, -1, role.getDataDirectory()), 1, wx.EXPAND | wx.ALL, 2)
            self.dataPanel.GetSizer().Add(hbox, 0, wx.EXPAND | wx.ALL, 2)
        
        self.deployButton = wx.Button(self, wx.ID_ANY, 'Start Deployment', size=(180, -1))
        self.GetSizer().Add(self.deployButton, 0, wx.ALL, 2)
        
        self.cancelButton = wx.Button(self, wx.ID_ANY, 'Cancel', size=(110, -1))
        self.GetSizer().Add(self.cancelButton, 0, wx.ALL, 2)
        
        self.cancelButton.Hide()
        self.update()
        
    def update(self):
        '''
        Update the panel to reflect the current Deployment
        '''
        self.statusField.SetLabel(self.deployment.state)
        
        if self.deployment.state != DeploymentState.NOT_RUN:
            self.deployButton.Hide()
            #self.cancelButton.Show()
        
        if self.deployment.state == DeploymentState.COMPLETED:
            self.cancelButton.Hide()
            self.dataPanel.Show()
        
        
        if self.deployment.state == DeploymentState.ROLES_STARTED:
            self.deployButton.SetLabel("Restore deployment")
            self.deployButton.Show()
        
