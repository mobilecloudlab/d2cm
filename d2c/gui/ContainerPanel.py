"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import wx

class ContainerPanel(wx.Panel):
    """
    Contains multiple panels in same position, with only one visible
    """
    
    def __init__(self, parent, id=-1, size=wx.DefaultSize):
        wx.Panel.__init__(self, parent, id=id, size=size)
        self._sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self._sizer)
        self._panels = {}
    
    def addPanel(self, label, panel):
        assert panel.GetParent() == self
        self._panels[label] = panel
        self._sizer.Add(panel, 1, wx.ALL|wx.EXPAND, 0)
        panel.Hide()
        
    def showPanel(self, label):
        
        if not self._panels.has_key(label):
            raise Exception("ContainerPanel does not have panel ID: %s" % label)
        
        for l, p in self._panels.items():
            if l == label:
                p.Show()
            elif p.IsShown():
                p.Hide()
        self.Layout()
    
    def getPanel(self, label):
        return self._panels[label]
    
    def removePanel(self, label):
        self._panels[label].Hide()
        del self._panels[label]
    
    def clearPanels(self):
        
        for panel in self._panels.values():
            panel.GetParent().RemoveChild(panel)