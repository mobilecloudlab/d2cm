"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import wx
from .ItemList import ColumnMapper, ItemList

class RoleList(ItemList):
    
    def __init__(self, *args, **kwargs):
        
        kwargs['mappers'] = [ColumnMapper('Name', lambda r: r.template.name, defaultWidth=wx.LIST_AUTOSIZE),
                             ColumnMapper('Image', lambda r: r.image.getDisplayName(), defaultWidth=wx.LIST_AUTOSIZE),
                             ColumnMapper('Count', lambda r: r.count, defaultWidth=wx.LIST_AUTOSIZE_USEHEADER),
                             ColumnMapper('Instance Type', lambda r: '' if r.instanceType is None else r.instanceType.name, defaultWidth=wx.LIST_AUTOSIZE_USEHEADER),
                             ColumnMapper('Output Data', lambda r: 'Click', defaultWidth=wx.LIST_AUTOSIZE_USEHEADER)]
        
        kwargs['style'] =wx.LC_REPORT
        ItemList.__init__(self, *args, **kwargs)
        
class RoleTemplateList(ItemList):
    
    def __init__(self, *args, **kwargs):
        
        kwargs['mappers'] = [ColumnMapper('Name', lambda r: r.name, defaultWidth=wx.LIST_AUTOSIZE)]
        kwargs['style'] =wx.LC_REPORT
       
        ItemList.__init__(self, *args, **kwargs)
        
        
                   
    