"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

'''
A utility class which installs collectd in supplied images.
'''
import libvirt

from .SourceImage import SourceImage
import libvirtmod
import re

class CollectdInstaller(object):
    '''
    TODO this class is not operable yet.
    '''
    def __init__(self):
        pass
    
    def install(self, img):

        assert isinstance(img, SourceImage)
      
        vConn = libvirt.open("vbox:///session")
        print vConn.numOfDefinedDomains()
      
        for d in vConn.listDefinedDomains():
            dom = vConn.lookupByName(d)
            if self._hasDisk(img.path, dom.XMLDesc(0)):
                print d
                print "Launching..."
                dom.create()
            
    def _hasDisk(self, diskFile, descriptionXml):
        #print descriptionXml
        if re.search(diskFile, descriptionXml):
            return True
        else:
            return False
    
