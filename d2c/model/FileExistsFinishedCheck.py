"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from d2c.RemoteShellExecutor import RemoteShellExecutor

class FileExistsFinishedCheck(object):

    def __init__(self, fileName, sshCred=None):
        
        self.fileName = fileName
        
        self.sshCred = sshCred
    
    class LineChecker:
        
        def __init__(self, match):
            self.match = match
            self.matchFound = False
            
        def write(self, line):
            if self.match == line.strip():
                self.matchFound = True
        
        
    def check(self, instance):
        '''
        Log into the instance via SSH and test self.fileName for existence.
        Return True if the file exists on the remote host.
        '''    
        
        checker = self.LineChecker('exists')
        cmd = "if [ -f %s ]; then echo exists; fi" % self.fileName
        RemoteShellExecutor(self.sshCred.username, 
                            instance.public_dns_name, 
						    self.sshCred.privateKey, 
                            outputLogger=checker).run(cmd)
        
        return checker.matchFound
    
    def copy(self):
        
        return FileExistsFinishedCheck(self.fileName)