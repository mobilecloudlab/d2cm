"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class AWSCred(object):
    """
    Stores Amazon Web Service credentials. These are used in all requests with AWS-compliant services.
    See: http://docs.amazonwebservices.com/AmazonS3/latest/dev/index.html?S3_Authentication.html
    """
    
    def __init__(self, name, aws_access_key_id, aws_secret_access_key):
        
        assert isinstance(aws_access_key_id, basestring)
        assert isinstance(aws_secret_access_key, basestring)
        self.name = name
        self.access_key_id = str(aws_access_key_id)
        self.secret_access_key = str(aws_secret_access_key)
        
    def __str__(self):
        return "AWSCred: \n\tkey_id:%s\nacces_key:\t%s" % (self.access_key_id, "HIDDEN_SECRET_KEY")