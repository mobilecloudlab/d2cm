"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from .Cloud import Cloud


class SourceImage(object):

    def __init__(self, id, image, cloud, dateAdded, size):
        
        assert id is None or isinstance(id, int)
        assert image is None or isinstance(image, Image)
        assert isinstance(cloud, Cloud)
        
        self.id = id
        self.image = image
        self.cloud = cloud
        self.dateAdded = dateAdded
        self.size = size
        
class DesktopImage(SourceImage):
    
    def __init__(self, id, image, cloud, dateAdded, size, sizeOnDisk, path):
        SourceImage.__init__(self, id, image, cloud, dateAdded, size)
        
        assert isinstance(path, basestring)
        self.sizeOnDisk = sizeOnDisk
        self.path = path

    def getDisplayName(self):
        return "%s:%s" % (self.image.name, self.path)
        
class Image(object):
    
    def __init__(self, id, name, originalImage, reals=()):
        
        assert id is None or isinstance(id, int)
        assert name is None or isinstance(name, basestring)
        assert isinstance(originalImage, SourceImage)
        
        self.id = id
        self.name = name
        self.originalImage = originalImage
        self.reals = list(reals)
        
        if self.originalImage not in self.reals:
            self.reals.append(self.originalImage)
        
    def getHostedSourceImage(self, cloud):
        '''
        Return the real image that is hosted on the cloud.
        '''
    
        assert isinstance(cloud, Cloud)
        
        for real in self.reals:
            if real.cloud is cloud:
                return real
        
        return None
    
class AMI(SourceImage):
    
    def __init__(self, id, image, amiId, cloud, dateAdded, size, kernel=None, ramdisk=None):
        SourceImage.__init__(self, id, image, cloud, dateAdded, size)
        self.amiId = amiId
        self.ramdisk = ramdisk
        self.kernel = kernel
        
    def getDisplayName(self):
        return "%s:%s" % (self.image.name, self.amiId)