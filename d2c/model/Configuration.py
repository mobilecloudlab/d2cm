"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import re

class ConfValidationError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value
        
    def __str__(self):
        return repr(self.value)

class Configuration(object):
    
    def __init__(self, awsUserId, ec2Cred, awsCred):
        #self.ec2ToolHome = ec2ToolHome
        self.awsUserId = awsUserId
        self.awsCred = awsCred
        self.ec2Cred = ec2Cred
        
    def validate(self):
        #self.__validateEc2ToolHome()
        self.__validateAwsUserId()
    
    '''    
    def __validateEc2ToolHome(self):
        if self.ec2ToolHome and not os.path.exists(self.ec2ToolHome):
            raise ConfValidationError("EC2 Tool Home directory not found: " + self.ec2ToolHome)
    '''
    
    def __validateAwsUserId(self):
        return self.awsUserId and re.match("\d{12}", self.awsUserId)
    
    def __str__(self):
        return "awsCred: %s, ec2Cred: %s" % (str(self.awsCred), str(self.ec2Cred))