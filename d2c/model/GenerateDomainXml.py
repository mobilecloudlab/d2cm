"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from xml.dom.minidom import parse
import os
import sys
import random
import pkg_resources

class GenerateXML(object):
    
    @staticmethod
    def randomizeName():
        s="abcdef123"
        return ''.join(random.sample(s,len(s)))
    
    @staticmethod   
    def generateXML(vdi_image,noOfCPU,memory):

        source_xml =  pkg_resources.resource_filename(__package__, "virtualbox_xml/mydomain.xml")
        
        # set path of vdi image
        doc = parse(source_xml)
        ref = doc.getElementsByTagName('source')[0]
        ref.attributes["file"]=vdi_image
        
        # set domain name
        node = doc.getElementsByTagName('name')
        node[0].firstChild.nodeValue = GenerateXML.randomizeName()
        
        # set cpu number
        node = doc.getElementsByTagName('vcpu')
        node[0].firstChild.nodeValue = noOfCPU
        
        
        # set memory
        node = doc.getElementsByTagName('memory')
        node[0].firstChild.nodeValue = memory
        
        # set current memory
        node = doc.getElementsByTagName('currentMemory')
        node[0].firstChild.nodeValue = memory
        
        ## persist changes to new file
        #xml_file = open(generated_xml_file, "w")
        #doc.writexml(xml_file, encoding="utf-8")
        #xml_file.close()
        print "generated xml file:"
        print doc.toxml()
        
        return doc.toxml()