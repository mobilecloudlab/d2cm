"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from d2c.logger import StdOutLogger
import os

class UploadAction(object):

    def __init__(self, 
                 source, 
                 destination,
                 sshCred,
                 logger=StdOutLogger()):
        
        assert isinstance(source, basestring) 
        assert len(source) > 0
        assert isinstance(destination, basestring)
        assert len(destination) > 0
    
        self.source = source
        self.destination = destination
        self.logger = logger
        self.sshCred = sshCred
        
    def copy(self):      
        return UploadAction(self.source, self.destination, self.sshCred)
     
    def execute(self, instance, shellVars):   
            
        #assert os.path.isdir(self.source) or os.path.isfile(self.source), "Source not present: %s" % str(self.source)    
       
        cmd = "scp -i %(key)s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no %(src)s %(user)s@%(host)s:%(dest)s" % \
            {'key':self.sshCred.privateKey,
             'src':self.source,
             'dest':self.destination,
             'user':self.sshCred.username,
             'host':instance.public_dns_name}
            
        self.executorFactory.executor(self.logger, self.logger).run(cmd)
