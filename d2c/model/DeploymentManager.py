"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from .Deployment import Deployment

class DeploymentManager:

    def __init__(self, dao, ec2ConnectionFactory, logger):
        self.dao = dao
        self.ec2ConnectionFactory = ec2ConnectionFactory
        self.logger = logger
    
    def reserveInstance(self, ec2Conn, role, count):
        
        self.logger.write("Reserving %d instance(s) of %s" % (count, role))
       
        r = ec2Conn.run_instances(role.ami.amiId, min_count=count, 
                                      max_count=count, instance_type='m1.large') #TODO unhardcode instance type
        
        self.logger.write("Instance(s) reserved")
        
        return r
    
    def deploy(self, deploymentDesc):
        
        self.logger.write("Getting EC2Connection")
        ec2Conn = self.ec2ConnectionFactory.getConnection()
        self.logger.write("Got EC2Connection")
        
        reservations = []
        
        for (role, count) in deploymentDesc.roleCountMap.iteritems():
            reservations.append(self.reserveInstance(ec2Conn, role, count))
            
        return Deployment(deploymentDesc, reservations)
  