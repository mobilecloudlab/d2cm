"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class Architecture(object):
    
    def __init__(self, arch):
        self.arch = arch

'''   
Architecture.X86 = Architecture('x86')
Architecture.X86_64 = Architecture('x86_64')
'''
        
class InstanceType(object):
    '''
    Represents EC2 instance type.
    http://aws.amazon.com/ec2/instance-types/
    '''
    
    def __init__(self, name, cpu, cpuCount, memory, disk, architectures, costPerHour):
        
        self.name = name
        self.cpu = cpu
        self.cpuCount = cpuCount
        self.memory = memory
        self.disk = disk
        self.architectures = list(architectures) if architectures is not None else list()
        self.costPerHour = costPerHour
    
'''        
InstanceType.T1_MICRO = InstanceType('t1.micro', 2, 2, 613, 0, (Architecture.X86, Architecture.X86_64), 0.025)
InstanceType.M1_SMALL = InstanceType('m1.small', 2, 1, 1700, 160, (Architecture.X86,), 0.095)
InstanceType.M1_LARGE = InstanceType('m1.large', 2, 2, 7500, 850, (Architecture.X86_64,), 0.038)
InstanceType.M1_XLARGE = InstanceType('m1.xlarge', 2, 4, 15000, 850, (Architecture.X86_64,), 0.76)


InstanceType.TYPES = {InstanceType.T1_MICRO.name: InstanceType.T1_MICRO, 
                      InstanceType.M1_SMALL.name: InstanceType.M1_SMALL,
                      InstanceType.M1_LARGE.name: InstanceType.M1_LARGE,
                      InstanceType.M1_XLARGE.name: InstanceType.M1_XLARGE}
'''
    