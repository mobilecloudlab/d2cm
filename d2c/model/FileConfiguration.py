"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from d2c.model.AWSCred import AWSCred
from d2c.model.EC2Cred import EC2Cred
from d2c.model.Configuration import Configuration
import string

class FileConfiguration(Configuration):
    '''
    Configuration settings that can be loaded from a simple 
    file for testing purposes.
    '''
    
    
    def __init__(self, confFile):
        settings = {}
        for l in open(confFile, "r"):
            (k, v) = string.split(l.strip(), "=")
            settings[k] = v
     
        ec2Cred = EC2Cred(settings['ec2CredId'], 
                          settings['cert'], 
                          settings['privateKey'])
    
        awsCred = AWSCred(settings['accessKey'],
                          settings['secretKey'])
        
        Configuration.__init__(self,
                               #ec2ToolHome='/opt/EC2_TOOLS',
                               awsUserId=settings['userid'],
                               ec2Cred=ec2Cred,
                               awsCred=awsCred)