"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os

from d2c.ShellExecutor import ShellExecutor
from d2c.model.AWSCred import AWSCred

class S3Storage:

    class BundleUploader:
        
        def __init__(self, serviceURL):
            self.serviceURL = serviceURL
            
    def __init__(self, name, serviceURL):
        self.name = name
        self.serviceURL = serviceURL
    
    def getServiceURL(self):
        return self.serviceURL
     
        
class AWSStorage(S3Storage):
    
    def __init__(self):
        S3Storage.__init__(self, "AWS S3", "https://s3.amazonaws.com")
    
    class AWSBundleUploader(S3Storage.BundleUploader):
        
        def __init__(self, serviceURL):
            S3Storage.BundleUploader.__init__(self, serviceURL)
        
        def upload(self, manifest, bucket, awsCred):
           
            assert isinstance(bucket, basestring)
            assert isinstance(manifest, basestring)
            assert isinstance(awsCred, AWSCred)
            UPLOAD_CMD = "ec2-upload-bundle --url %s -b %s -m %s -a %s -s %s"
        
            uploadCmd = UPLOAD_CMD % (self.serviceURL,
                                      bucket, manifest,
                                      awsCred.access_key_id,
                                      awsCred.secret_access_key)
        
            ShellExecutor().run(uploadCmd)
            
            return bucket + "/" + os.path.basename(manifest)
        
        
    def bundleUploader(self):
        return self.AWSBundleUploader(self.serviceURL)
        
class WalrusStorage(S3Storage):
    
    def __init__(self, name, serviceURL):
        S3Storage.__init__(self, name, serviceURL)
        
    class EucBundleUploader(S3Storage.BundleUploader):
        
        def __init__(self, serviceURL):
            S3Storage.BundleUploader.__init__(self, serviceURL)
            
        def upload(self, manifest, bucket, awsCred):
           
            assert isinstance(bucket, basestring)
            assert isinstance(manifest, basestring)
            assert isinstance(awsCred, AWSCred)
           
            UPLOAD_CMD = "euca-upload-bundle --url %s -b %s -m %s -a %s -s %s"
        
            uploadCmd = UPLOAD_CMD % (self.serviceURL,
                                      bucket, manifest,
                                      awsCred.access_key_id,
                                      awsCred.secret_access_key)
        
            ShellExecutor().run(uploadCmd)
            
            return bucket + "/" + os.path.basename(manifest)
                 
        
    def bundleUploader(self):
        return self.EucBundleUploader(self.serviceURL)
        
    
        