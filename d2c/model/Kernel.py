"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import re
import pkg_resources

class Kernel(object):
    
    
    def __init__(self, aki, architecture, 
                 contents, cloud=None, recommendedRamdisk=None, isPvGrub=False):
        self.aki = aki
        self.architecture = architecture
        self.contents = contents #Kernel Modules and optional GRUB
        self.cloud = cloud
        self.recommendedRamdisk = recommendedRamdisk
        self.isPvGrub = isPvGrub
        
    def getContentsAbsPath(self):
        '''
        Return absolute path of contents
        '''
        
        if self.contents is None:
            return None
        
        m = re.match("internal\:\/\/(.*)", self.contents)
        if m:
            return pkg_resources.resource_filename(__package__, m.group(1))
        else:
            return self.contents
        
    def __str__(self):
        return self.aki
    
    def __eq__(self, other):
        return (self.aki, self.cloud.name) == (other.aki, other.cloud.name)
    
    def __ne__(self, other):
        return not self.__eq__(other)

        
        
        