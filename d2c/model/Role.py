"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from d2c.logger import StdOutLogger   
from d2c.model.InstanceType import InstanceType
from d2c.model.Action import Action
from d2c.model.Deployment import Deployment
from d2c.model.AWSCred import AWSCred
import string
import os, sys
from copy import copy
from d2c.RRDFetch import RRDFetch
from .SSHCred import SSHCred

import time   

class Role(object):
    
    # Information used for Monitoring the instance with collectd
    RRD_FOLDER = 'tmp'
    CD_FOLDER = '/var/lib/collectd'
    SRC_FOLDER = '/tmp/collectd'
    HOUR = 3600
    
    def __init__(self,  
                 id, 
                 image, 
                 count,
                 instanceType, 
                 template,
                 deployment=None,
                 reservationId=None,
                 remoteExecutorFactory=None,
                 startActions=(),
                 asyncStartActions=(), 
                 uploadActions=(),
                 finishedChecks=(), 
                 dataCollectors=(),
                 pollRate=15,
                 logger=StdOutLogger(),
                 monitoringActivated=False,
                 sshCred=None):
        
        assert count > 0, "Count must be int > 0"
        assert instanceType is None or isinstance(instanceType, InstanceType), "Type is %s" % type(instanceType)
        assert deployment is None or isinstance(deployment, Deployment)
        
        self.id = id
        self.image = image  
        self.template = template
        self.count = count
        self.instanceType = instanceType
        self.pollRate = pollRate
        self.reservationId = reservationId
        self.reservation = None #lazy loaded
        self.deployment = deployment
        self.remoteExecutorFactory = remoteExecutorFactory
        self.startActions= list(startActions)
        self.asyncStartActions = list(asyncStartActions)
        self.uploadActions= list(uploadActions)
        self.finishedChecks= list(finishedChecks)
        self.dataCollectors= list(dataCollectors)
        self.logger = logger
        self.sshCred = sshCred
        self.monitoringActivated =  monitoringActivated
        
        
      
            
        
    def clone(self):
        '''
        Make a clone of this Role. Id will be set to None.
        Actions will be cloned as well.
        '''
        c = Role(None, image=self.image, count=self.count, instanceType=self.instanceType, 
                    template=self.template)
        
        c.startActions = [a.copy() for a in self.startActions]
        for a in c.startActions:
            a.id = None
            
        c.asyncStartActions = [a.copy() for a in self.asyncStartActions]
        for a in c.asyncStartActions:
            a.id = None
            
        c.uploadActions = [a.copy() for a in self.uploadActions]
        for a in c.uploadActions:
            a.id = None
            
        c.finishedChecks = [a.copy() for a in self.finishedChecks]
        for a in c.finishedChecks:
            a.id = None
            
        c.dataCollectors = [a.copy() for a in self.dataCollectors]
        for a in c.dataCollectors:
            a.id = None
        
        return c
    
    def getName(self):
        return self.template.name
    
    def getDataDirectory(self):
        return os.path.join(self.deployment.dataDir, str(self.id))
    
    def setSSHCred(self, sshCred):
        self.sshCred = sshCred
        
        for collection in [self.startActions, self.asyncStartActions, self.uploadActions, self.finishedChecks, self.dataCollectors]:
            for a in collection:
                a.sshCred = sshCred
    
    def setLogger(self, logger): 
    
        self.logger = logger
        
        self._cascadeLogger()
        
    def _cascadeLogger(self):
        
        for actions in [self.startActions, self.uploadActions, 
                        self.finishedChecks, 
                        self.dataCollectors]:
            for a in actions:
                a.logger = self.logger
      
    def getReservationId(self):  
        return self.reservationId
    
    def setReservationId(self, id):
        self.reservationId = id
    
    def getCount(self):
        return self.count
     
    def costPerHour(self):
        return self.count * self.instanceType.costPerHour if self.instanceType is not None else 0
       
    def launch(self, awsCred):
        
        assert awsCred is None or isinstance(awsCred, AWSCred)
        
        if self.count == 0:
            return
        
        cloudConn = self.deployment.cloud.getConnection(awsCred)
       
        self.logger.write("Reserving %d instance(s) of %s with launchKey %s" % (self.count, str(self.image.image.name), self.sshCred.name))
       
        #TODO catch exceptions     
        self.reservation = cloudConn.runInstances(self.image, 
                                                 count=self.count, 
                                                 instanceType=self.instanceType,
                                                 keyName=self.sshCred.name) 
        
        #TODO introduce abstraction-appropriate exception
        assert self.reservation is not None and self.reservation.id is not None
        
        self.reservationId = self.reservation.id
        
        self.logger.write("Instance(s) reserved with ID: %s" % self.reservationId)    
     
    def __executeActions(self, actions, shellVars=None): 
        '''
        Execute the actions on each instance within the role.
        '''
        
        if self.count == 0:
            return
        
        if  not hasattr(self, 'reservation') and self.reservation is None:
            self.reservation = self.__getReservation()
        
        for instance in self.reservation.instances: 
            instance.update()
            
            for action in actions:
                action.execute(instance, shellVars)
                
    def enableSshConnections(self, shellVars=None):
     
        
        cmd = "ssh-keyscan -f /tmp/d2c.context -t rsa,dsa >> /home/%s/.ssh/known_hosts" % self.sshCred.username
        #cmd = 'rm -f %s/*/*/*.rrd' % (self.SRC_FOLDER)
        
        action = Action(command=cmd, 
                        sshCred=self.sshCred)
        
        action.remoteExecutorFactory = self.remoteExecutorFactory
        
        for instance in self.reservation.instances:
            action.execute(instance, shellVars)
            
    
    def setPublicIPContext(self, ips):
        '''
        In the current implementation, this method will create file 
        /tmp/d2c.contextPublic
        on all hosts with a '\n' delimited line of public DNS of the instance
        This will be reworked in the the future to create a more full-featured 
        context scheme.
        '''
        
        if self.count == 0:
            return
        
        ctxt = string.join(ips, "\n")
        
        
        cmd = "echo -e \"%s\" > /tmp/d2c.contextPublic" % ctxt
        #cmd = 'rm -f %s/*/*/*.rrd' % (self.SRC_FOLDER)
        
        action = Action(command=cmd, 
                        sshCred=self.sshCred)
        action.remoteExecutorFactory = self.remoteExecutorFactory
        
        for instance in self.reservation.instances:
            action.execute(instance)
            
        ipFileName = os.path.join(self.getDataDirectory(), "ips.txt")
         
        print ipFileName
        os.system("mkdir -p %s" % str(self.getDataDirectory()))
        os.system("echo \"%s\" > %s" % (ctxt,ipFileName))
         
        #myFile = open(ipFileName,'rw+')
        #myFile.write(ctxt) #
        #myFile.close()
        
        
        
        
    
    def setIPContext(self, ips):
        '''
        In the current implementation, this method will create file 
        /tmp/d2c.context
        on all hosts with a '\n' delimited line of private IPs.
        This will be reworked in the the future to create a more full-featured 
        context scheme.
        '''
        
        if self.count == 0:
            return
        
        ctxt = string.join(ips, "\n")
        cmd = "echo -e \"%s\" > /tmp/d2c.context" % ctxt
        
        action = Action(command=cmd, 
                        sshCred=self.sshCred)
        action.remoteExecutorFactory = self.remoteExecutorFactory
        
        for instance in self.reservation.instances:
            action.execute(instance)
                
    def getPrivateIPs(self):
        '''
        Return a collection of the Private IPs of the instances associated with the role.
        '''
        
        if  not hasattr(self, 'reservation') or self.reservation is None:
            self.reservation = self.__getReservation()
        
        for instance in self.reservation.instances: 
            instance.update()
        
        return [str(i.private_ip_address) if i.private_ip_address is not None else str(i.private_dns_name) 
                    for i in self.reservation.instances]
        
    def getPublicIPs(self):
        '''
        Return a collection of the Public DNS IPs of the instances associated with the role.
        '''
        
        if  not hasattr(self, 'reservation') or self.reservation is None:
            self.reservation = self.__getReservation()
        
        for instance in self.reservation.instances: 
            instance.update()
        
        return [str(i.public_dns_name) if i.public_dns_name is not None else str(i.public_dns_name) 
                    for i in self.reservation.instances]
        
    def executeStartCommands(self, shellVars):
        '''
        Execute the start action(s) on each instance within the role.
        '''
        self.__executeActions(self.uploadActions, shellVars)
        self.__executeActions(self.startActions, shellVars)
        
    def executeAsyncStartCommands(self, shellVars):
        '''
        Execute the asynchronous start action(s) on each instance within the role.
        '''
        self.__executeActions(self.asyncStartActions, shellVars)
                
    def executeStopCommands(self):
        '''
        Execute the end action(s) on each instance within the role.
        '''
          
        self.__executeActions(self.stopActions) 
                
    def checkFinished(self):
        '''
        Connect to remote instances associated with this role. If each instance satisfies the done condition,
        return True, else return False. 
        '''
        
        if self.count == 0:
            return True
        
        if  not hasattr(self, 'reservation') or self.reservation is None:
            self.reservation = self.__getReservation()
            
        for instance in self.reservation.instances:
            for check in self.finishedChecks:
                if not check.check(instance):
                    return False
                
        self.logger.write("Role " + self.template.name + " Returning true for finished test")        
        
        return True
        
    def __getReservation(self):
        '''
        Update the reservation field with current information from AWS.
        '''    
        
        assert hasattr(self, 'reservationId') and self.reservationId is not None
        
        reservations = self.deployment.cloud.getConnection(self.deployment.awsCred).getAllInstances(self.reservationId)
        
        assert len(reservations) == 1, "Unable to retrieve reservation %s" % self.reservationId
        return reservations[0] 
        
    def collectData(self):
        
        if self.count == 0:
            return
        
        if not hasattr(self, 'reservation') or self.reservation is None:
            self.reservation = self.__getReservation()
        
        for collector in self.dataCollectors:
            for instance in self.reservation.instances:
                
                dest = os.path.join(self.getDataDirectory(), str(instance.id), collector.source[1:])
                
                self.logger.write("Downloading data from instance %s to %s... " % (instance.id, dest))
                collector.collect(instance, 
                                  dest)
                self.logger.write("Done")
                
    def getInstanceDataDirs(self):
        '''
        Returns iterable of full directory paths storing data of instances bound to this role.
        '''
        
        if not os.path.isdir(self.getDataDirectory()):
            return []
        
        baseDir = self.getDataDirectory()
        
        out = [os.path.join(baseDir,dir) for dir in os.listdir(baseDir) 
                if os.path.isdir(os.path.join(baseDir, dir))]
        
        print len(out)
        return out
    
    def getIntsanceCollectdDirs(self):
        
        out = []
        for dataDir in self.getInstanceDataDirs():
            collectdDir = os.path.join(dataDir, 'opt/collectd/var/lib/collectd')
            print collectdDir
            if os.path.isdir(collectdDir):
                #Get host dir
                paths = os.listdir(collectdDir)
                if len(paths) == 1 and os.path.isdir(os.path.join(collectdDir, paths[0])):
                    out.append(os.path.join(collectdDir, paths[0]))
                    
        return out
    
    #def setInstallCollectd(self, ips):
    def setInstallCollectd(self):
        '''
        In the current implementation, this method will install collectd-core 
        in the intances
        on all hosts
        This will be reworked in the the future to create a more full-featured class that
        take care of this task.
        '''
        
        if self.count == 0:
            return
        
        #ctxt = string.join(ips, "\n")
        #sudoCmd = "sudo -i"
        collectdCmd = "sudo apt-get install -y collectd-core"
        
        #cmd = "echo -e \"%s\" > /tmp/d2c.context" % ctxt
        
        #actionSudo = Action(command=sudoCmd, sshCred=self.sshCred)
        actionInstall = Action(command=collectdCmd, sshCred=self.sshCred)
        
        #action = Action(command=cmd, sshCred=self.sshCred)
        
        #action.remoteExecutorFactory = self.remoteExecutorFactory
        
        #actionSudo.remoteExecutorFactory = self.remoteExecutorFactory
        actionInstall.remoteExecutorFactory = self.remoteExecutorFactory
        
        for instance in self.reservation.instances:
            actionInstall.execute(instance)
        
        #sleep(10)
    
    def getSetCollectdConfig(self):
        #str = ["FQDNLookup true", "BaseDir \"/var/lib/collectd\"", "Interval 10", "Timeout 2", "LoadPlugin syslog", "<Plugin syslog>", "LogLevel info", "</Plugin>", "LoadPlugin cpu", "LoadPlugin load", "LoadPlugin memory", "LoadPlugin rrdtool"]
        strConf = ["FQDNLookup true", "Interval 10", "Timeout 2", "LoadPlugin syslog", "<Plugin syslog>", "LogLevel info", "</Plugin>", "LoadPlugin cpu", "LoadPlugin load", "LoadPlugin memory", "LoadPlugin rrdtool"]
        
        ctxt = string.join(strConf, "\n")
        cmd = "echo -e \"%s\" | sudo tee /etc/collectd/collectd.conf" % ctxt
        
        action = Action(command=cmd, sshCred=self.sshCred)
        action.remoteExecutorFactory = self.remoteExecutorFactory
        
        for instance in self.reservation.instances:
            action.execute(instance)
    
    def getStartCollectdService(self):    
        
        cmd = "sudo service collectd start"
        
        action = Action(command=cmd, sshCred=self.sshCred)
        action.remoteExecutorFactory = self.remoteExecutorFactory
        
        for instance in self.reservation.instances:
            action.execute(instance)

    def getMonitorCollectedData(self):    
        
        cmd = 'cp -r %s /tmp' % self.CD_FOLDER
        
        action = Action(command=cmd, sshCred=self.sshCred)
        action.remoteExecutorFactory = self.remoteExecutorFactory
        
        for instance in self.reservation.instances:
            action.execute(instance)
            
        """
        cmd = 'for i in %s/*/*/*.rrd; do rrdtool dump $i > $i.xml ; done' % (self.SRC_FOLDER)
        
        action = Action(command=cmd, sshCred=self.sshCred)
        action.remoteExecutorFactory = self.remoteExecutorFactory
        
        for instance in self.reservation.instances:
            action.execute(instance)
        
        cmd = 'rm -f %s/*/*/*.rrd' % (self.SRC_FOLDER)
        
        action = Action(command=cmd, sshCred=self.sshCred)
        action.remoteExecutorFactory = self.remoteExecutorFactory
        
        for instance in self.reservation.instances:
            action.execute(instance)
        """
        
        cmd = 'tar cvzf %s.tar.gz %s' % (self.SRC_FOLDER, self.SRC_FOLDER)
        
        action = Action(command=cmd, sshCred=self.sshCred)
        action.remoteExecutorFactory = self.remoteExecutorFactory
        
        for instance in self.reservation.instances:
            action.execute(instance)
        
        for instance in self.reservation.instances:
            collectdDir = os.path.join(self.getDataDirectory(), instance.id)
            collectdDir = os.path.join(collectdDir, 'opt/collectd/var/lib')
            #collectdDir = os.path.join(collectdDir, self.RRD_FOLDER)
            
            print collectdDir
            cmd = "mkdir -p %s" % (collectdDir)
            print cmd
            os.system(cmd)
            
            cmd = "scp -r -i %s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no %s@%s:%s.tar.gz* %s/" % (self.sshCred.privateKey, self.sshCred.username, instance.public_dns_name, self.SRC_FOLDER, collectdDir)
            print cmd
            os.system(cmd)
            
            cmd = "tar xvf %s/collectd.tar.gz -C %s" % (collectdDir, collectdDir)
            print cmd
            os.system(cmd)
            
            """
            cmd = "for i in %s/%s/*/*/*/*.xml; do rrdtool restore -f $i ${i%%.*} ; done" % (collectdDir, self.RRD_FOLDER)
            print cmd
            os.system(cmd)
            
            cmd = 'rm -f %s/%s/*/*/*/*.xml' % (collectdDir, self.RRD_FOLDER)
            print cmd
            os.system(cmd)
            """
            
            #Move files to graphic directory
            #cmd = "mv -f %s/%s/collectd/%s %s/%s/collectd/dirac.lan " % (collectdDir, self.RRD_FOLDER, instance.private_dns_name, collectdDir, self.RRD_FOLDER)
            cmd = "cp -r %s/%s/collectd/%s %s/%s/collectd/dirac.lan " % (collectdDir, self.RRD_FOLDER, instance.private_dns_name, collectdDir, self.RRD_FOLDER)
            print cmd
            os.system(cmd)
            
            cmd = "rm -rf %s/%s/collectd/%s" % (collectdDir, self.RRD_FOLDER, instance.private_dns_name)
            print cmd
            os.system(cmd)

            #cmd = "mv -f %s/%s/collectd %s " % (collectdDir, self.RRD_FOLDER, collectdDir)
            cmd = "cp -r %s/%s/collectd %s " % (collectdDir, self.RRD_FOLDER, collectdDir)
            print cmd
            os.system(cmd)
            
            cmd = "rm -rf %s/%s/collectd" % (collectdDir, self.RRD_FOLDER)
            print cmd
            os.system(cmd)

            cmd = "rm -rf %s/%s " % (collectdDir, self.RRD_FOLDER)
            print cmd
            os.system(cmd)

            cmd = "rm -rf %s/collectd.tar.gz " % (collectdDir)
            print cmd
            os.system(cmd)
    

    def testFunc(self):
        for instance in self.reservation.instances:
            collectdDir = os.path.join(self.getDataDirectory(), instance.id)
            collectdDir = os.path.join(collectdDir, 'opt/collectd/var/lib')
            #collectdDir = os.path.join(collectdDir, self.RRD_FOLDER)
            os.system("mkdir -p %s" % (collectdDir))
            print collectdDir
    
    def shutdown(self):
        
        
        if  not hasattr(self, 'reservation') or self.reservation is None:
            self.reservation = self.__getReservation()
        
        #Request the instances be terminated  
        for instance in self.reservation.instances:
            
            #Boto before:
            #instance.stop()
            #Boto 2.0+ :
            
            try:
                instance.terminate()
            except Exception as x:
                instance.stop()
        
        #Monitor until all are terminated
        monitorInstances = self.reservation.instances
        
        while len(monitorInstances) > 0:
            for instance in self.reservation.instances:
                instance.update()
                print instance.state
            monitorInstances = filter(lambda inst: inst.state != 'terminated', monitorInstances) 
            
            #for instance in self.reservation.instances:
            #    instance.terminate()
                
            time.sleep(self.pollRate)
        
        self.logger.write("Reservation %s successfully terminated" % self.reservation.id)
        
    def __str__(self):
        return "{id:%s, image: %s}" % (self.id, self.image)
