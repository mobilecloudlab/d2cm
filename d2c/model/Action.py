"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from d2c.logger import StdOutLogger
from d2c.RemoteShellExecutor import RemoteShellExecutor

class Action(object):

    def __init__(self, 
                 command, 
                 sshCred,
                 logger=StdOutLogger()):
        
        assert isinstance(command, basestring)
        
        self.command = command
        self.sshCred = sshCred
        self.logger = logger
        
    def copy(self):
        return Action(self.command, self.sshCred)
     
    def execute(self, instance, shellVars=None):   
        
        shellStr = '';
        if shellVars is not None:
            shellStr = ' '
            for k,v in shellVars.iteritems():
                shellStr = k + "=" + str(v) + " " + shellStr
            
        self.remoteExecutorFactory.executor(self.sshCred.username, 
                            instance.public_dns_name, 
                            self.sshCred.privateKey).run(shellStr + self.command)
                            
                      
class StartAction(Action):

    def __init__(self, 
                 command, 
                 sshCred=None,
                 logger=StdOutLogger()):
        
        Action.__init__(self, command, 
                 sshCred,
                 logger)
        
    def copy(self):
        return StartAction(self.command)

        