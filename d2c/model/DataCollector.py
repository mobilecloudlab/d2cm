"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from d2c.ShellExecutor import ShellExecutor
from d2c.logger import StdOutLogger
import os


class DataCollector(object):

    def __init__(self, 
                 source, 
                 sshCred=None,
                 logger=StdOutLogger()):
        
        assert isinstance(source, basestring) and source[0] == '/'
        
        self.source = source
        self.logger = logger
        self.sshCred = sshCred
        
    def copy(self):
        return DataCollector(self.source, self.sshCred)
     
    def collect(self, instance, destination): 
        
        destDir = os.path.dirname(destination)
        if not os.path.exists(destDir):
            os.makedirs(destDir)
        
        cmd = "scp -r -i '%s' -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no %s@%s:%s '%s'" % (self.sshCred.privateKey, 
                                                                        self.sshCred.username,
                                                                        instance.public_dns_name, 
                                                                        self.source,
                                                                        destination)
        self.executorFactory.executor(self.logger).run(cmd)