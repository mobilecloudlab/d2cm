"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class EC2Cred(object):
    
    cert = None #file path
    private_key = None #file path
    
    def __init__(self, id, ec2_cert, ec2_private_key):   
        self.id = id
        self.cert = ec2_cert
        self.private_key = ec2_private_key
        
    def __str__(self):
        return "EC2Cred: \n'tid: %s\n\tcert: %s\n\tprivate_key: %s" % (self.id, self.cert, self.private_key)