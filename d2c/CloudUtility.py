"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# Utility used to run commands against the cloud and get the output
# The output is processed according to class of the d2c.model
from d2c.ShellExecutor import ShellExecutor
from d2c.model.Kernel import Kernel
from d2c.model.Ramdisk import Ramdisk
from d2c.model.SourceImage import DesktopImage, Image, AMI
from logger import StdOutLogger

from d2c.model.InstanceType import InstanceType, Architecture

from d2c.CloudParseUtility import CloudParseUtility

class CloudUtility(object):
    def __init__(self, serviceURL):
        self.serviceURL = serviceURL
    
    def getDescribeImages(self, awsCred):
        #DESCRIBE_CMD = "euca-upload-bundle --url %s -b %s -m %s -a %s -s %s"
        DESCRIBE_CMD = "euca-describe-images --url %s -A %s -S %s"
        
        describeCmd = DESCRIBE_CMD % (self.serviceURL, awsCred.access_key_id, awsCred.secret_access_key)
        
        outVal = CloudOutput()
        
        #TODO: Put the ShellExecutor in a asynchronous thread with timeout - Otherwise the application hangs if it cannot connect
        ShellExecutor(outVal, StdOutLogger()).run(describeCmd)
        
        parseUtil = CloudParseUtility(outVal.msg)
        
        print parseUtil.getAMIList()
    
    def getKernelList(self):
        return 0
    
    def getRamdiskList(self):
        return 0
    
    def getAMIList(self):
        return 0
    
    def get_instance_types(self, dao):
    
        X86 = dao.getArchitecture('x86')
        X86_64 = dao.getArchitecture('x86_64')
        
        return [InstanceType('t1.micro', 2, 2, 613, 0, (X86, X86_64), 0.025),
                InstanceType('m1.small', 1, 1, 1700, 150, (X86, X86_64), 0.090),
                InstanceType('m1.medium', 2, 1, 3750, 400, (X86, X86_64), 0.360),
                InstanceType('m1.large', 2, 2, 7500, 840, (X86_64,), 0.360),
                InstanceType('m1.xlarge', 2, 4, 15000, 1680, (X86_64,), 0.720),
                InstanceType('c1.medium', 2, 2, 1700, 340, (X86, X86_64), 0.186),
                InstanceType('c1.xlarge', 2, 8, 7000, 1680, (X86_64,), 0.744),
                InstanceType('m2.xlarge', 3, 2, 17100, 410, (X86_64,), 0.506),
                InstanceType('m2.2xlarge', 3, 4, 34200, 840, (X86_64,), 1.012),
                InstanceType('m2.4xlarge', 3, 8, 68400, 1680, (X86_64,), 2.024),
                InstanceType('cc1.4xlarge', 8, 4, 23000, 1690, (X86_64,), 1.300),
                InstanceType('cc2.8xlarge', 11, 8, 60500, 3360, (X86_64,), 2.400),
                InstanceType('cg1.4xlarge', 8, 4, 22000, 1680, (X86_64,), 2.100)]
    
class CloudOutput():
    def __init__(self):
        self.msg = ""
    
    def write(self, msg):
        self.msg = self.msg + msg + "\n"
        