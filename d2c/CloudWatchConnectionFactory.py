"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from boto.ec2.cloudwatch import CloudWatchConnection
from logger import StdOutLogger

class CloudWatchConnectionFactory:
    
    
    def __init__(self, credStore, logger=StdOutLogger()):
        self.__logger = logger
        self.__credStore = credStore
        self.__conn = None
        
    def getConnection(self):
        
        #TODO worry about thread safety?
        #TODO un-hardcode
        region = "eu-west-1"
         
        cred = self.__credStore.getDefaultAWSCred()
        
        self.__logger.write("Acquiring CloudWatch connection to region %s" % region)
        
        if self.__conn is None:
            self.__conn = CloudWatchConnection(host='monitoring.%s.amazonaws.com' % region,
                                               aws_access_key_id=cred.access_key_id,
                                               aws_secret_access_key=cred.secret_access_key)
        
        self.__logger.write("CloudWatch connection acquired to region %s" % region)
        
        return self.__conn
    