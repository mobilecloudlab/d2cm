"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from logger import StdOutLogger
from subprocess import Popen
import subprocess
import sys

class ShellExecutorFactory(object):

    def executor(self, outputLogger=StdOutLogger(),
                       logger=StdOutLogger()):
        return ShellExecutor(outputLogger, logger)

class ShellExecutor(object):

    def __init__(self, outputLogger=StdOutLogger(),
                       logger=StdOutLogger()):
        self.__logger = logger
        self.outputLogger = outputLogger
       
    def run(self, cmd):
        
        self.__logger.write("Executing: " + cmd)    
        
        
        p = Popen(cmd, shell=True,
                  stdout=subprocess.PIPE, 
                  stderr=subprocess.STDOUT, 
                  close_fds=True)
        
        while True:
            line = p.stdout.readline()
            if not line: break
            self.outputLogger.write(line)
    
        # This call will block until process finishes and p.returncode is set.
        p.wait()
        
        if 0 != p.returncode:
            raise Exception("Command \"%s\"failed with code %d '" % (cmd, p.returncode))

            