"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import sys, os
import glob, re
from d2c.RRDGraph import RRDGraph

class RRDFetch:
    """
        Class for gathering all the rrd files from the file system and draw stacked graphs using RRDGraph class
    """
    rrd_folder = '' # where to search the files
    rrd_folder_pattern = '' # which metrics we like to plot (folder name, allows patterns, can use 'cpu-*' to find cpu-0, cpu-1 etc)
    rrd_files = list() # maintain list of files to be used for plotting
    rrd_files_columns = list() # maintain list of columns in the files to be used for plotting

    """
        Constructor for defining where to search the RRD files. 
            e.g.
                rrd_folder = '/var/lib/collectd/rrd/'
                rrd_folder_pattern 'cpu-*/'
    """
    def __init__(self, rrd_folder = "", rrd_folder_pattern = ""):
        self.rrd_folder = rrd_folder
        self.rrd_folder_pattern = rrd_folder_pattern
        self.rrd_files = list()
        self.rrd_files_columns = list()

    """
        Searches RRD files from given folder, in this way you can gather
        different RRD files from different folders.
    """
    def findFilesFromFolder(self, rrd_folder, rrd_folder_pattern):
        self.rrd_folder = rrd_folder
        self.rrd_folder_pattern = rrd_folder_pattern
        self.findFiles()

    """
        Finds all the necessary rrd files for plotting
    """
    def findFiles(self):
        #for infile in glob.glob( os.path.join(self.rrd_folder, '*/', self.rrd_folder_pattern, '*.rrd') ):
        for infile in glob.glob( os.path.join(self.rrd_folder, self.rrd_folder_pattern, '*.rrd') ):
            tmp = re.search('([a-z]+)\.rrd', infile)
            if tmp != None:
                if tmp.group(1) != 'idle': # ignore idle column
                    self.rrd_files.append(infile)
                    self.rrd_files_columns.append(tmp.group(1))

    """
        Draws the graph, calls out RRDGraph for drawing
    """
    def drawGraph(self, output, title = '', label = '', startString = '', endString = '', width = 400, height = 300, smooth = False, divide_values = False, maximum = -1):
            if len(self.rrd_files) == 0: # check, whatever we found any files for drawing the graph or not
                raise Exception('Cannot plot graph, as there are no RRD files found from the system')
            rrdGraph = RRDGraph(self.rrd_files, self.rrd_files_columns)
            rrdGraph.drawGraph(output, title, label, startString, endString, width, height, smooth, divide_values, maximum)

"""
if __name__ == '__main__':
    FOLDER1 = '/var/lib/collectd/rrd/' # subfolder for this folder should be IP address 
    FOLDER2 = '/home/oinas/Desktop/RRDPython/letter2/download/'
    HOUR = 3600
    time = HOUR * 4

    # make new RRDFetch class, it will empty the list (for each measurement type (CPU, memory, load), create a new class)
    rrdFetch = RRDFetch()
    # first place to search CPU RRD files
    rrdFetch.findFilesFromFolder(FOLDER1, "cpu-*/")
    # second place to search CPU RRD files
    rrdFetch.findFilesFromFolder(FOLDER2, "cpu-*/")
    # draw the graph
    rrdFetch.drawGraph('merged_cpu.png', 'Servers CPU usage', 'CPU percentage', 
            time, 600, 400, False, True, 100)

            
    rrdFetch = RRDFetch()
    rrdFetch.findFilesFromFolder(FOLDER1, "memory/")
    rrdFetch.findFilesFromFolder(FOLDER2, "memory/")
    rrdFetch.drawGraph('merged_memory.png', 'Servers memory usage', 'Total memory usage', 
            time, 600, 400, False, False)

            
    rrdFetch = RRDFetch()
    rrdFetch.findFilesFromFolder(FOLDER1, "load/")
    rrdFetch.findFilesFromFolder(FOLDER2, "load/")
    rrdFetch.drawGraph('merged_load.png', 'Servers load', '1 minute load', 
            time, 600, 400, False, False)
"""
    