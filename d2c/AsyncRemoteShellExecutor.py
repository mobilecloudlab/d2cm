"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from .RemoteShellExecutor import RemoteShellExecutor
from .ShellExecutor import ShellExecutor
from .logger import StdOutLogger

import string
import random

class AsyncRemoteShellExecutor(RemoteShellExecutor):
    
    def __init__(self, user, host, 
                 privateKey=None, 
                 outputLogger=StdOutLogger(),
                 logger=StdOutLogger()):
        
        RemoteShellExecutor.__init__(self, user, host, 
                                    privateKey, 
                                    outputLogger,
                                    logger)
        
    def run(self, cmd):
        '''
        Executes remote commands in a nohup sh wrapper. The run method will not block, and cmd 
        will continue on the remote machine.
        '''
                
        cmd = string.replace(cmd, "\\", "\\\\")
        cmd = string.replace(cmd, "\"", "\\\"")
        
        CMD_WRAPPER = "nohup sh -c \"%s\" &>/tmp/hup.%d.out < /dev/null &"
        
        cmd = CMD_WRAPPER % (cmd, random.randint(1,10000))
        
        RemoteShellExecutor.run(self, cmd)
       