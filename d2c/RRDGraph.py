"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import sys, os

class RRDGraph:
    """ 
        Class for drawing graph using rrdtool
    """
    rrd_files = list()    # will hold list of RRD files to be used when drawing the graph
    rrd_files_columns = list()    # for each file, define column name
    columns = {}    # holds unique column names (used for stacking same values together)
    # holds list of colors (hexadecimal) for each graph
    rrd_colors = ['777777','0000ff','00ff00','ff0000','00ffff','ffff00','ff00ff',
        '000077','007700','770000','007777','777700','770077']

    """
        Constructor for defining the RRD files and their columns
    """
    def __init__(self, rrd_files, rrd_files_columns):
        if len(rrd_files) != len(rrd_files_columns):
            raise Exception('Lists rrd_files and rrd_files_columns has to have same amount of elements')
        self.rrd_files = rrd_files
        self.rrd_files_columns = rrd_files_columns
        self.columns = {}
        for i in self.unique(self.rrd_files_columns):    # keep indices in the hash map
            self.columns[i] = 0

    """
        Search unique column names, this is used, when drawing the graph and putting 
        same RRD files together (e.g. combining cpu-0/cpu-user.rrd and cpu-1/cpu-user.rrd)
    """
    def unique(self, list):
        checked = []
        for e in list:
            if e not in checked:
                checked.append(e)    
        return checked

    """
        Draws the graph, executes RRDTool from command line
    """
    def drawGraph(self, output, title = '', label = '', startString = '', endString = '', width = 400, height = 300, smooth = False, divide_values = False, maximum = -1):
        #def drawGraph(self, output, title = '', label = '', time = 3600, width = 400, height = 300, smooth = False, divide_values = False, maximum = -1):
        parameters = list()    # holds list of parameters that are used for drawing the graphs
        parameters.append(output)
        parameters.append('--imgformat')
        parameters.append('PNG')
        parameters.append('--width')
        parameters.append('%i' % width)
        parameters.append('--height')
        parameters.append('%i' % height)
        parameters.append('--start')
        #parameters.append('-%i' % time)
        parameters.append('%s' % str(startString))
        parameters.append('--end')
        #parameters.append('-30')
        parameters.append('%s' % str(endString))
        parameters.append('--lower-limit')
        parameters.append('0')
        if maximum > 0:    #we have defined upper limit of the graph
            parameters.append('--upper-limit')
            parameters.append('%i' % maximum)
            parameters.append( 'HRULE:%i#770000' % maximum )    # draw the maximum line
        parameters.append('--title')
        parameters.append(title)
        parameters.append('--vertical-label')
        parameters.append(label)
        
        if smooth:    # if we want smooth graph, add parameter -E
            parameters.append('-E')

        i = 0
        for rrd_file in self.rrd_files:
            tmp = self.rrd_files_columns[i]    # get the column name
            
            if tmp == 'load':    # load stores 3 values in one file (shortterm - 1min, medterm - 5min, longterm - 15min)
                rrd_ds_name = 'shortterm'
            else:
                rrd_ds_name = 'value'
            
            # define the data type
            parameters.append('DEF:%s%i=%s:%s:AVERAGE' % 
                (tmp, self.columns[tmp], rrd_file, rrd_ds_name) )

            # increment counters
            self.columns[tmp] += 1
            i += 1

        if divide_values:
            key = self.rrd_files_columns[0]    # calculate, how many server we are running for each time period
            tmp = list()
            for i in range(self.columns[key]):
                tmp.insert(0, '%s%i,UN,0,1,IF' % (key, i))
                if i != 0:
                    tmp.append('+')
            parameters.append( 'CDEF:srv_count=%s' % ','.join(tmp) )
            
        for key in self.columns:
            tmp = list()
            for i in range(self.columns[key]):
                # http://oss.oetiker.ch/rrdtool/tut/cdeftutorial.en.html read manual, how var,UN,0,var,IF will change unknown values to 0
                tmp.insert(0, '%s%i,UN,0,%s%i,IF' % (key, i, key, i))
                if i != 0:
                    tmp.append('+')    # RPN calculation
            if divide_values:
                tmp.append('srv_count,/')
            parameters.append( 'CDEF:%s=%s' % (key, ','.join(tmp)) )

        i = 0
        for key in self.columns:    # draw grahp areas (preserve the order)
            parameters.append('AREA:%s#%s:%s:STACK' %
                (key, self.rrd_colors[i], key) )
            i += 1        
            
        
        cmd = "rrdtool graph '%s'" % "' '".join(parameters)    # run the command
        print cmd
        os.system(cmd)
"""
if __name__ == '__main__':
    HOUR = 3600
    time = HOUR * 3
    rrd_files = [
            'download/ip-10-111-61-228.ec2.internal/cpu-0/cpu-user.rrd',
            'download/ip-10-111-61-228.ec2.internal/cpu-0/cpu-system.rrd',
            'download/ip-10-111-61-228.ec2.internal/cpu-0/cpu-nice.rrd',
            'download/ip-10-111-61-228.ec2.internal/cpu-0/cpu-wait.rrd',
            'download/ip-10-111-61-228.ec2.internal/cpu-0/cpu-steal.rrd'
        ]
    rrd_files_columns = ['user','system','nice','wait','steal']
    rrdGraph = RRDGraph(rrd_files, rrd_files_columns)
    rrdGraph.drawGraph('single_cpu.png', 'Servers CPU usage', 'CPU percentage', time)
    
    rrd_files = [
            'download/ip-10-111-61-228.ec2.internal/memory/memory-used.rrd',
            'download/ip-10-111-61-228.ec2.internal/memory/memory-free.rrd',
            'download/ip-10-111-61-228.ec2.internal/memory/memory-buffered.rrd',
            'download/ip-10-111-61-228.ec2.internal/memory/memory-cached.rrd'
        ]
    rrd_files_columns = ['used','free','buffered','cached']
    rrdGraph = RRDGraph(rrd_files, rrd_files_columns)
    rrdGraph.drawGraph('single_memory.png', 'Servers memory usage', 'memory', time)
    """