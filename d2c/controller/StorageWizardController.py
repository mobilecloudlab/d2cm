"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import wx


class StorageWizardController:
    
    def __init__(self, view, dao):
        
        self._view = view
        self._dao = dao
         
        self._view.container.getPanel("MAIN").setStores(dao.getImageStores())
        self._view.showPanel("MAIN") 
        
        self._view.container.getPanel("MAIN").addButton.Bind(wx.EVT_BUTTON, self.showPanel("NEW_STORE"))
        self._view.container.getPanel("NEW_STORE").addButton.Bind(wx.EVT_BUTTON, self.addStore)
        
        self._view.container.getPanel("MAIN").doneButton.Bind(wx.EVT_BUTTON, lambda _: self._view.EndModal(wx.ID_OK))
    
    def showPanel(self, label):
        return lambda _: self._view.showPanel(label)
    
    def addStore(self, _):
        
        newStorePanel = self._view.container.getPanel("NEW_STORE")
        try:
            store = newStorePanel.getStore()
        except Exception as x:
            wx.MessageBox(x.message, 'Info')
            return
        
        newStorePanel.clear()
        
        self._dao.addImageStore(store)
        self._view.container.getPanel("MAIN").addStore(store)
        self._view.showPanel("MAIN")