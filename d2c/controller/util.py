"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import wx

def __activateOnNotEmpty(control, *fields):
    '''
    Iterates through all fields.
    If all fields' values are not empty, control is enabled, else disabled.
    '''
    for f in fields:
        if __empty(f):
            control.Disable()
            return False
           
    control.Enable() 
    return True
    
def __empty(field):
    if isinstance(field, (wx.TextCtrl, wx.ComboBox)):
        return len(field.GetValue()) == 0
    if isinstance(field, wx.ListCtrl):
        return field.GetSelectedItemCount() == 0
    
    return False
    
def createEmptyChecker(control, *fields):
    checker = lambda _: __activateOnNotEmpty(control, *fields)
    for field in fields:
        if isinstance(field, (wx.TextCtrl, wx.ComboBox)):
            field.Bind(wx.EVT_TEXT, checker)
        if isinstance(field, wx.ListCtrl):
            field.Bind(wx.EVT_LIST_ITEM_SELECTED, checker)
            field.Bind(wx.EVT_LIST_ITEM_DESELECTED, checker)
    checker(None)
    