"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import wx

from .AMIWizardController import AMIWizardController
from d2c.gui.NewAMIWizard import NewAMIWizard
from d2c.model.SourceImage import SourceImage

from .util import createEmptyChecker  

class ImageController:
    
    def __init__(self, imageView, dao):
        
        self._imageView = imageView
        self._dao = dao
          
        self._imageView.addButton.Bind(wx.EVT_BUTTON, self._onAddImage) 
        self._imageView.SetImages(dao.getDesktopImages())
        self._imageView.createAMIButton.Bind(wx.EVT_BUTTON, self._createAMIImage)
        
        createEmptyChecker(self._imageView.addButton, self._imageView.newFileText)
        createEmptyChecker(self._imageView.createAMIButton, self._imageView.list)
        
    def _onAddImage(self, _):
        
        path = self._imageView.newFileText.GetValue()
        
        if path == "":
            wx.MessageBox('Path must not be empty', 'Info')
            return

        self._dao.add(SourceImage(path))
        self._refreshImgList()
        
    def _refreshImgList(self):
        self._imageView.SetImages(self._dao.getSourceImages())
        
    def _createAMIImage(self, _):
        
        img = self._imageView.list.getSelectedItems()[0]
        amiWiz = NewAMIWizard(None, -1, 'Create AMI', size=(600,300))
        
        controller = AMIWizardController(amiWiz, self._dao)
        controller.setImage(img)
        
        amiWiz.ShowModal()
        amiWiz.Destroy()
        