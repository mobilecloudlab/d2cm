"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import wx
#from d2c.model.Cloud import Cloud
import boto
from d2c.model.Cloud import EC2Cloud, Cloud
from d2c.model.Kernel import Kernel
from d2c.CloudUtility import CloudUtility

def fieldsNotEmpty(self, *fields):
        for f in fields:
            if len(f.GetValue()) == 0:
                return False
            
        return True

class NewCloudController:
    
    def __init__(self, view, dao):
        
        self._view = view
        self._dao = dao
         
        self._view.container.getPanel("MAIN").cloudList.setItems(self._dao.getClouds()) 
         
        self._view.container.getPanel("MAIN").doneButton.Bind(wx.EVT_BUTTON, self.done)
        self._view.container.getPanel("MAIN").addButton.Bind(wx.EVT_BUTTON, self.showAddCloud)
        
        
        self._view.container.getPanel("NEW_CLOUD").name.Bind(wx.EVT_TEXT, self.checkCanSaveCloud)
        self._view.container.getPanel("NEW_CLOUD").serviceURL.Bind(wx.EVT_TEXT, self.checkCanSaveCloud)
        self._view.container.getPanel("NEW_CLOUD").storageURL.Bind(wx.EVT_TEXT, self.checkCanSaveCloud)
        self._view.container.getPanel("NEW_CLOUD").ec2Cert.Bind(wx.EVT_TEXT, self.checkCanSaveCloud)
        
        self._view.container.getPanel("NEW_CLOUD").kernelId.Bind(wx.EVT_TEXT, self.checkCanAddKernel)
        self._view.container.getPanel("NEW_CLOUD").kernelData.Bind(wx.EVT_TEXT, self.checkCanAddKernel)
        
        self._view.container.getPanel("NEW_CLOUD").addKernelButton.Bind(wx.EVT_BUTTON, self.addKernel)
        self._view.container.getPanel("NEW_CLOUD").saveButton.Bind(wx.EVT_BUTTON, self.save)
        self._view.container.getPanel("NEW_CLOUD").closeButton.Bind(wx.EVT_BUTTON, self.close)
        
        
        self._view.container.showPanel("MAIN")
    
    def showAddCloud(self, _):
        self._view.container.showPanel("NEW_CLOUD")
    
    def done(self, _):
        self._view.EndModal(wx.ID_OK)
        
    def close(self,_):
        self._view.container.showPanel("MAIN")
    
    def save(self, _):
        
        panel = self._view.container.getPanel("NEW_CLOUD")
        
        cloudUtility = CloudUtility(panel.serviceURL.GetValue())
        #cloud = Cloud(panel.name.GetValue(), panel.serviceURL.GetValue(), 
        #              panel.storageURL.GetValue(), panel.ec2Cert.GetValue())
        cloud = EC2Cloud(None, 
                       name=panel.name.GetValue(), 
                        serviceURL=panel.serviceURL.GetValue(),
                        ec2Cert=panel.ec2Cert.GetValue(),
                        storageURL=panel.storageURL.GetValue(),
                        kernels=panel.kernelList.getItems(),
                        instanceTypes= cloudUtility.get_instance_types(self._dao),
                        botoModule=boto)
        #cloud = EC2Cloud(panel.name.GetValue(), panel.serviceURL.GetValue(), panel.storageURL.GetValue(), panel.ec2Cert.GetValue())
        
        cloud.kernels = panel.kernelList.getItems()
        
        #self._dao.saveCloud(cloud)
        
        self._dao.add(cloud)
        
        self._view.container.getPanel("MAIN").cloudList.addItem(cloud)
        self._view.container.showPanel("MAIN")
        
    
    def addKernel(self, _):
        panel = self._view.container.getPanel("NEW_CLOUD")
        panel.kernelList.addItem(Kernel(panel.kernelId.GetValue(), 
                                        self._dao.getArchitecture('x86_64'), panel.kernelData.GetValue()))
        
        
    def checkCanAddKernel(self, _):
        
        panel = self._view.container.getPanel("NEW_CLOUD")
        
        if fieldsNotEmpty(panel.kernelId,
                          panel.kernelData):
            panel.addKernelButton.Enable() 
        else:
            panel.addKernelButton.Disable()
            
    def checkCanSaveCloud(self, _):
        panel = self._view.container.getPanel("NEW_CLOUD")
        
        if fieldsNotEmpty(panel.name, panel.serviceURL, 
                          panel.storageURL, panel.ec2Cert):
            panel.saveButton.Enable()
        else:
            panel.saveButton.Disable()
            
    