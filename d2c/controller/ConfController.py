"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import wx
from d2c.model.EC2Cred import EC2Cred
from d2c.model.AWSCred import AWSCred
from d2c.model.Configuration import Configuration

class ConfController:
    
    def __init__(self, credView, dao):
        
        self._credView = credView
        self._dao = dao
        
        credView.addButton.Bind(wx.EVT_BUTTON, self._onAdd)
        credView.closeButton.Bind(wx.EVT_BUTTON, self._onClose)
        
    def _onClose(self, _):
        self._credView.close()
        
    def _onAdd(self, _):
        self._credView.addCred()
        
    def _onSave(self, event):
        

        awsCred = AWSCred("mainKey", self._credView._aws_key_id.GetValue(),
                          self._credView._aws_secret_access_key.GetValue())
        
        ec2Cred = EC2Cred("defaultEC2Cred", self._credView._ec2_cert.GetValue(),
                self._credView._ec2_private_key.GetValue())
        
        awsUserId = self._credView.aws_user_id.GetValue()
        
        conf = Configuration(awsUserId=awsUserId,
                             ec2Cred=ec2Cred,
                             awsCred=awsCred)
        
        self._dao.saveConfiguration(conf)
        
        self._credView.EndModal(wx.ID_OK)
