"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from .ShellExecutor import ShellExecutor
from .logger import StdOutLogger

import string

class RemoteShellExecutorFactory(object):

    def executor(self, user, host, 
                 privateKey=None, 
                 outputLogger=StdOutLogger(),
                 logger=StdOutLogger()):
        
        return RemoteShellExecutor(user, host, 
                                   privateKey, 
                                   outputLogger,
                                   logger)

class RemoteShellExecutor(ShellExecutor):
    
    def __init__(self, user, host, 
                 privateKey=None, 
                 outputLogger=StdOutLogger(),
                 logger=StdOutLogger()):
        
        ShellExecutor.__init__(self, outputLogger, logger)
        self.user = user
        self.host = host
        self.privateKey = privateKey
        
    def run(self, cmd):
        
        pKeyStr = "-i '%s'" % self.privateKey if self.privateKey else ""
        cmd = string.replace(cmd, "\\", "\\\\")
        cmd = string.replace(cmd, "\"", "\\\"")
        cmd = string.replace(cmd, "`", "\`")
        cmd = string.replace(cmd, "$", "\$")
        cmd = "ssh %s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no %s@%s \"%s\"" % (pKeyStr, 
                                                                 self.user,
                                                                 self.host,
                                                                 cmd)
        
        ShellExecutor.run(self, cmd)
    