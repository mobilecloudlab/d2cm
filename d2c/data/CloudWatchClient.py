"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from d2c.data.InstanceMetrics import Metric, MetricValue, MetricList, InstanceMetrics

class CloudWatchClient:


    def __init__(self, cwConnFactory, dao):
        
        self.cwConnFactory = cwConnFactory
        self.dao = dao
        
    
    def getInstanceMetrics(self, instanceId, startTime, endTime):    
        conn = self.cwConnFactory.getConnection()
    
        mets = []
        
        for metric in self.dao.getMetrics():
            mResp = conn.get_metric_statistics(period=300, 
                                       start_time=startTime, 
                                       end_time=endTime, 
                                       metric_name=metric.name, 
                                       namespace="AWS/EC2", 
                                       statistics='Sum', 
                                       dimensions={"InstanceId":instanceId}, 
                                       unit=metric.unit)
            mets.append(self.__map(instanceId, metric, mResp))
          
        return InstanceMetrics(instanceId, mets)
                
    def __map(self, instanceId, metric, mResp):
        '''
        Map a metric api response to a metric list
        '''
        values = []
        for slice in mResp:
            values.append(MetricValue(slice['Sum'], slice['Timestamp']))
        
        return MetricList(instanceId, metric, values)
            
            
            