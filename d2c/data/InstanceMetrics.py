"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class Metric:
    
    def __init__(self, name, unit):
        self.name = name
        self.unit = unit
        
    def __str__(self):
        return "name: %s; unit: %s" % (self.name, self.unit)

class MetricValue:

    def __init__(self, value, time):
        self.value = value
        self.time = time
        
    def __str__(self):
        return "Value: %d; Time: %s" % (self.value, self.time)
    
class MetricList:
    
    def __init__(self, instanceId, metric, values):
        self.instanceId = instanceId
        self.metric = metric
        self.values = sorted(values, key=lambda v : v.time)
        
    def __str__(self):
        out = "Metric = %s\n" % self.metric
        out += "Values = "
        for v in self.values:
            out += "%s\n" % str(v)
        
        return out

class InstanceMetrics:
    
    def __init__(self, instanceId, metricLists):
        self.instanceId = instanceId
        self.metricLists = metricLists
        
    def __str__(self):
        out = "instanceId : " + self.instanceId
        out += "\nmetricLists : \n"
        
        for l in self.metricLists:
            out += "\t%s\n" % l
        
        return out
        
