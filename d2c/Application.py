"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import wx
from wx.lib.pubsub import Publisher

from d2c.gui.Gui import Gui
from d2c.controller.ConfController import ConfController

from d2c.gui.ConfPanel import CredDialog
from d2c.controller.ImageController import ImageController
from d2c.controller.AMIController import AMIController
from d2c.gui.DeploymentTemplateWizard import DeploymentTemplateWizard
from d2c.controller.DeploymentTemplateWizardController import DeploymentTemplateWizardController
from d2c.controller.DeploymentController import DeploymentController, DeploymentTemplateController
from d2c.controller.NewCloudController import NewCloudController
from d2c.gui.CloudPanel import CloudWizard
from d2c.gui.DeploymentTab import DeploymentTemplatePanel



class Application:

    def __init__(self, dao, amiToolsFactory):
        
        self._amiToolsFactory = amiToolsFactory
        self._dao = dao
        self._app = wx.App()

        self._frame = Gui(dao)
    
        self._imageController = ImageController(self._frame.imagePanel, self._dao)
        self._amiController = AMIController(self._frame.amiPanel, 
                                            self._dao,
                                            self._amiToolsFactory)
        
        self.loadDeploymentPanels()
        
        self._frame.bindAddDeploymentTool(self.addDeployment)
        self._frame.bindConfTool(self.showConf)
        self._frame.bindCloudTool(self.showCloudWizard)
        
        self._frame.deploymentPanel.tree.Bind(wx.EVT_TREE_SEL_CHANGED, self.deploymentSelect)
        
        Publisher.subscribe(self._handleNewDeploymentTemplate, "DEPLOYMENT TEMPLATE CREATED")
        Publisher.subscribe(self._handleNewDeployment, "DEPLOYMENT CREATED")
        Publisher.subscribe(self._handleDeleteDeployment, "DELETE DEPLOYMENT")
        
        self._frame
        self._frame.Show()     
            
    def deploymentSelect(self, event):
        self._frame.deploymentPanel.displayPanel.showPanel(self._frame.deploymentPanel.tree.GetItemText(event.GetItem()))
    
    def _handleDeleteDeployment(self, msg):    
        deployment = msg.data['deployment']
        self._frame.deploymentPanel.removeDeployment(deployment)
        self._dao.delete(deployment)
    
    def _handleNewDeploymentTemplate(self, msg):    
        deployment = msg.data['deployment']
        self.loadDeploymentPanel(deployment)  
        
    def _handleNewDeployment(self, msg):    
        deployment = msg.data['deployment']
        self._frame.deploymentPanel.addDeployment(deployment)      
        
    def loadDeploymentPanels(self):
        self.deplomentControllers = {}
        for d in self._dao.getDeploymentTemplates():
            self.loadDeploymentPanel(d)
            
    def loadDeploymentPanel(self, deployment):
        
        deployPanel = DeploymentTemplatePanel(deployment, self._dao, self._frame.deploymentPanel.displayPanel)
        self._frame.deploymentPanel.addDeploymentTemplatePanel(deployPanel)
        self.deplomentControllers[deployment.id] = DeploymentTemplateController(deployPanel, self._dao)
    
    def addDeployment(self, event):
        mywiz = DeploymentTemplateWizard(None, -1, 'Deployment Template Creation Wizard')
        DeploymentTemplateWizardController(mywiz, self._dao)
        mywiz.ShowModal()
        mywiz.Destroy()
        
    def showConf(self, event):
        
        conf = CredDialog(self._dao, None, size=(800,400))
        ConfController(conf, self._dao)
        conf.ShowModal()
        conf.Destroy()
        
    def showCloudWizard(self, event):
        
        cloudWiz = CloudWizard(None, -1, 'Manage Clouds', size=(500,400))
        NewCloudController(cloudWiz, self._dao)
        cloudWiz.ShowModal()
        cloudWiz.Destroy()
        
    def MainLoop(self):
        self._app.MainLoop()
        
        
