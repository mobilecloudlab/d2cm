"""
    This file is part of Desktop to Cloud Migration (D2CM) tool for 
    migrating scientific applications to cloud. 

    Copyright (C) 2011    Chris Willmore
    Copyright (C) 2012    Pelle Jakovits, Satish Srirama

    D2CM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# This utility is get the inputData from the output of executing the euca-describe-images
# and process it to return the list of kernels, ramdisks or machines (AMIs)
# This lists still are in raw state, which means that the items contains in the lists is all the information returned by
# the output of the command, in the following order.
#
# Kernel and ramdisk lists data by item:
# [IMAGE, amiId, manifest, user, status, priviledges, architecture, storeType, instance-sotre]
#
# Machine (AMIs) list data by item:
# [IMAGE, amiId, manifest, user, status, priviledges, architecture, storeType, kernelId, ramdiskId, instance-sotre]

class CloudParseUtility(object):
    def __init__(self, rawString):
        self.lines = rawString.split("\n")
        self.min = 8
    
    def getListOfType(self, storeType):
        # storeType: Type of store in the cloud ( machine | kernel | ramdisk)
        # used with the data retrieved from the command "euca-describe-images"
        lines = [l.split() for l in self.lines if len(l.split()) >= self.min]
        lns = []
        for l in lines:
            if l[7] == storeType:
                lns.append(l)
                
        return lns
    
    def getKernelList(self):
        kernelLines = [kl.split() for kl in self.lines if len(kl.split()) >= self.min]
        ks = []
        for k in kernelLines:
            if k[7] == "kernel":
                ks.append(k)
        
        return ks
    
    def getRamdiskList(self):
        ramdiskLines = [rl.split() for rl in self.lines if len(rl.split()) >= self.min]
        rs = []
        for r in ramdiskLines:
            if r[7] == "ramdisk":
                rs.append(r)
        
        return rs
    
    def getAMIList(self):
        amiLines = [aml.split() for aml in self.lines if len(aml.split()) >= self.min]
        amis = []
        for a in amiLines:
            if a[7] == "machine":
                amis.append(a)
        
        return amis
        